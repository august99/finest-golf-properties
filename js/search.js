
jQuery(document).ready(function($){
	$(".psst").live('change', function(){
		var select = $(this);
		var id=$(this).attr('id');
		var vu=$(this).val();
		var p=$(this).parent().parent().attr('id');
		console.log(id+ " " + vu + " -- parent --" + p);
		if(vu!=''){
			if(id=='pscountry'){
				var city_options = '<option value="">Select City</option>';
				var state_options = '<option value="">Select State</option>';
				var if_us = 'no';
				if(vu==446) {
					if_us = 'yes';
				}
				$("#cdcity").hide();
				$("#cdstate").hide();
				$.ajax({
					type: 'POST',
					url: ajax.url,
					data: ({action: 'rem_get_cities', vu:vu, if_us:if_us }),
					success: function(response) { //response is state
						if(if_us != 'yes') {
							$('#pscity').empty();
						
							var obj = JSON.parse(response);

							$.each(obj, function(k,v) {
								city_options += '<option value="'+k+'">'+v+'</option>';
							});

							$('#pscity').html(city_options);
							$("#cdstate").hide();
							$("#cdcity").fadeIn(400);

						} else {
							$('#psstate').empty();
						
							var obj2 = JSON.parse(response);

							$.each(obj2, function(k,v) {
								state_options += '<option value="'+k+'">'+v+'</option>';
							});

							$('#psstate').html(state_options);
							$("#cdcity").hide();
							$("#cdstate").fadeIn(400);
						}
						
					}

				});
			}else if(id=='psstate'){
				var city_options2 = '<option value="">Select City</option>';
				var country_id = $('#pscountry').val();
				var state_id = $('#psstate').val();
				$("#cdcity").hide();
				$.ajax({
					type: 'POST',
					url: ajax.url,
					data: ({action: 'rem_get_country_cities_with_state', country_id:country_id, state_id:state_id }),
					success: function(response) { //response is city under particular state
						$('#pscity').empty();
						
						var obj3 = JSON.parse(response);

						$.each(obj3, function(k,v) {
							city_options2 += '<option value="'+k+'">'+v+'</option>';
						});

						$('#pscity').html(city_options2);
						$("#cdcity").fadeIn(400);
					}
				});
			}else if(id=='pscity'){
				$("#cdptype").fadeIn(400);
			}else if(id=='pstype'){
				$("#cdboxes").fadeIn(400);
			}

			

			if(id=='dscountry'){
				var city_options = '<option value="">Select City</option>';
				var state_options = '<option value="">Select State</option>';
				var if_us = 'no';
				if(vu==446) {
					if_us = 'yes';
				}
				$("#dscityc").hide();
				$("#dsstatec").hide();
				$.ajax({
					type: 'POST',
					url: ajax.url,
					data: ({action: 'rem_get_cities', vu:vu, if_us:if_us }),
					success: function(response) { //response is state
						if(if_us != 'yes') {
							$('#dscity').empty();
						
							var obj = JSON.parse(response);

							$.each(obj, function(k,v) {
								city_options += '<option value="'+k+'">'+v+'</option>';
							});

							$('#dscity').html(city_options);
							$("#dsstatec").hide();
							$("#dscityc").fadeIn(400);

						} else {
							$('#dsstate').empty();
						
							var obj2 = JSON.parse(response);

							$.each(obj2, function(k,v) {
								state_options += '<option value="'+k+'">'+v+'</option>';
							});

							$('#dsstate').html(state_options);
							$("#dscityc").hide();
							$("#dsstatec").fadeIn(400);
						}
						
					}

				});

				
			}else if(id=='dsstate') {
				var city_options2 = '<option value="">Select City</option>';
				var country_id = $('#dscountry').val();
				var state_id = $('#dsstate').val();
				$("#dscityc").hide();
				$.ajax({
					type: 'POST',
					url: ajax.url,
					data: ({action: 'rem_get_country_cities_with_state', country_id:country_id, state_id:state_id }),
					success: function(response) { //response is city under particular state
						$('#dscity').empty();
						
						var obj3 = JSON.parse(response);

						$.each(obj3, function(k,v) {
							city_options2 += '<option value="'+k+'">'+v+'</option>';
						});

						$('#dscity').html(city_options2);
						$("#dscityc").fadeIn(400);
					}
				});

			}else if(id=='dscity'){
				$("#dsboxesc").fadeIn(400);
			}

			if(id=='gcountry'){
				var city_options = '<option value="">Select City</option>';
				var state_options = '<option value="">Select State</option>';
				var if_us = 'no';
				if(vu==446) {
					if_us = 'yes';
				}
				$("#gcityc").hide();
				$("#gstatec").hide();
				$.ajax({
					type: 'POST',
					url: ajax.url,
					data: ({action: 'rem_get_cities', vu:vu, if_us:if_us }),
					success: function(response) { //response is state
						if(if_us != 'yes') {
							$('#gcity').empty();
						
							var obj = JSON.parse(response);

							$.each(obj, function(k,v) {
								city_options += '<option value="'+k+'">'+v+'</option>';
							});

							$('#gcity').html(city_options);
							$("#gstatec").hide();
							$("#gcityc").fadeIn(400);

						} else {
							$('#gstate').empty();
						
							var obj2 = JSON.parse(response);

							$.each(obj2, function(k,v) {
								state_options += '<option value="'+k+'">'+v+'</option>';
							});

							$('#gstate').html(state_options);
							$("#gcityc").hide();
							$("#gstatec").fadeIn(400);
						}
						
					}

				});
			}else if(id=='gstate'){
				var city_options2 = '<option value="">Select City</option>';
				var country_id = $('#gcountry').val();
				var state_id = $('#gstate').val();
				$("#gcityc").hide();
				$.ajax({
					type: 'POST',
					url: ajax.url,
					data: ({action: 'rem_get_country_cities_with_state', country_id:country_id, state_id:state_id }),
					success: function(response) { //response is city under particular state
						$('#gcity').empty();
						
						var obj3 = JSON.parse(response);

						$.each(obj3, function(k,v) {
							city_options2 += '<option value="'+k+'">'+v+'</option>';
						});

						$('#gcity').html(city_options2);
						$("#gcityc").fadeIn(400);
					}
				});
				// else if(id=='gcity'){
				// 	$("#gcommunityc").fadeIn(400);
				// }
			}else if(id=='gcity'){
				$("#gboxesc").fadeIn(400);
			}

			if(id=='acountry'){

				var city_options = '<option value="">Select City</option>';
				var state_options = '<option value="">Select State</option>';
				var if_us = 'no';
				if(vu==446) {
					if_us = 'yes';
				}
				$("#acityc").hide();
				$("#astatec").hide();
				$.ajax({
					type: 'POST',
					url: ajax.url,
					data: ({action: 'rem_get_cities', vu:vu, if_us:if_us }),
					success: function(response) { //response is state
						if(if_us != 'yes') {
							$('#acity').empty();
						
							var obj = JSON.parse(response);

							$.each(obj, function(k,v) {
								city_options += '<option value="'+k+'">'+v+'</option>';
							});

							$('#acity').html(city_options);
							$("#astatec").hide();
							$("#acityc").fadeIn(400);

						} else {
							$('#astate').empty();
						
							var obj2 = JSON.parse(response);

							$.each(obj2, function(k,v) {
								state_options += '<option value="'+k+'">'+v+'</option>';
							});

							$('#astate').html(state_options);
							$("#acityc").hide();
							$("#astatec").fadeIn(400);
						}
						
					}

				});

			}else if(id=='astate'){
				var city_options2 = '<option value="">Select City</option>';
				var country_id = $('#acountry').val();
				var state_id = $('#astate').val();
				$("#acityc").hide();
				$.ajax({
					type: 'POST',
					url: ajax.url,
					data: ({action: 'rem_get_country_cities_with_state', country_id:country_id, state_id:state_id }),
					success: function(response) { //response is city under particular state
						$('#acity').empty();
						
						var obj3 = JSON.parse(response);

						$.each(obj3, function(k,v) {
							city_options2 += '<option value="'+k+'">'+v+'</option>';
						});

						$('#acity').html(city_options2);
						$("#acityc").fadeIn(400);
					}
				});
			}else if(id=='acity'){
				$("#aboxesc").fadeIn(400);
			}


			if(id=='caname'){
				$("#cnamec").fadeIn(400);
			}


		}else{
			switch(id){

				case 'caname':
						$("#cnamec").hide();
						break;

				case 'pscountry':
						$("#cdstate").hide();
				case 'psstate':
						$("#cdcity").hide();
				case 'pscity':
						$("#cdptype").hide();
				case 'pstype':
						$("#cdboxes").hide();
						break;

				case 'dscountry':
						$("#dsstatec").hide();
				case 'dsstate':
						$("#dscityc").hide();
				case 'dscity':
						$("#dsboxesc").hide();
						break;

				case 'acountry':
						$("#astatec").hide();
				case 'astate':
						$("#acityc").hide();
				case 'acity':
						$("#acommunityc").hide();
				case 'acommunity':
						$("#aboxesc").hide();
						break;

				case 'gcountry':
						$("#gstatec").hide();
				case 'gstate':
						$("#gcityc").hide();
				case 'gcity':
						$("#gcommunityc").hide();
				case 'gcommunity':
						$("#gboxesc").hide();
						break;


			}
		}
	});

	$('#convert-item-price').change(function(){
		hayme_convert_price($(this).val());
	});

	$('#search-results-box select').change(function(){
		$('#search-results-ordered').html('Loading result..');
		hayme_order_search_result($(this).val());
	});

	hayme_order_search_result($('#hayme_order_search_result').val());
});

//ajax request
function hayme_order_search_result(order)
{
	jQuery('#hayme_order_search_form input[name=action]').val('hayme_order_search_result');
	jQuery.ajax({
		type: 'POST',
		url: ajax.url,
		data: jQuery("#hayme_order_search_form").serialize(),
		success: function(response) {
			jQuery('#search-results-ordered').html(response);
			jQuery("#search-results-box").mCustomScrollbar("update");
		},
		error: function() {
			console.log('Failed to order search result.');
		}
	});
}

function hayme_save_property_to_session(key) {
	jQuery('#hayme_order_search_form input[name=action]').val('hayme_save_property_to_session');
	jQuery('#hayme_order_search_form input[name=listingKey]').val(key);
	jQuery.ajax({
		type: 'POST',
		url: ajax.url,
		//data: ({action: 'hayme_save_property_to_session', listingkey:key }),
		data: jQuery("#hayme_order_search_form").serialize()+'&listingKey='+key,
		success: function(response) {
			window.location = response;
		},
		error: function() {
			console.log('Failed to save property to session.');
		}
	});
}

function hayme_convert_price(xdata) {
	jQuery.ajax({
		type: 'POST',
		url: ajax.url,
		data: ({action: 'hayme_convert_price', from:jQuery('#current-convertion').val(), to:xdata, price:jQuery('#current-price').val() }),
		success: function(response) {
			jQuery('#current-convertion').val(xdata);
			jQuery('.item-price').html('$ '+response+' '+xdata);
		},
		error: function() {
			console.log('Failed to save property to session.');
		}
	});
}

