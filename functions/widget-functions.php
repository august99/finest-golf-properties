<?php

/*
 * Widget Functions
 */


//Welcome Panel
if ( function_exists('register_sidebar') )
	register_sidebar(array(
	'name' => 'Welcome Panel',
	'before_widget' => '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">',
	'after_widget' => '</div>',
	'before_title' => '<h2 class="heading main-heading">',
	'after_title' => '</h2>',
)); 


//HP Tagline
if ( function_exists('register_sidebar') )
	register_sidebar(array(
	'name' => 'HP Tagline',
	'before_widget' => '',
	'after_widget' => '',
	'before_title' => '<h2>',
	'after_title' => '</h2>',
)); 


//Contact #
if ( function_exists('register_sidebar') )
	register_sidebar(array(
	'name' => 'Contact Number',
	'before_widget' => '',
	'after_widget' => '',
	'before_title' => '',
	'after_title' => '',
));

//Contact Email
if ( function_exists('register_sidebar') )
	register_sidebar(array(
	'name' => 'Contact Email',
	'before_widget' => '',
	'after_widget' => '',
	'before_title' => '',
	'after_title' => '',
)); 

//FB Link
if ( function_exists('register_sidebar') )
	register_sidebar(array(
	'name' => 'FB Link',
	'before_widget' => '',
	'after_widget' => '',
	'before_title' => '',
	'after_title' => '',
)); 

//Twitter Link
if ( function_exists('register_sidebar') )
	register_sidebar(array(
	'name' => 'Twitter Link',
	'before_widget' => '',
	'after_widget' => '',
	'before_title' => '',
	'after_title' => '',
)); 

//Pinterest Link
if ( function_exists('register_sidebar') )
	register_sidebar(array(
	'name' => 'Pinterest Link',
	'before_widget' => '',
	'after_widget' => '',
	'before_title' => '',
	'after_title' => '',
)); 


//Copyright Info
if ( function_exists('register_sidebar') )
	register_sidebar(array(
	'name' => 'Copyright Info',
	'before_widget' => '',
	'after_widget' => '',
	'before_title' => '',
	'after_title' => '',
)); 