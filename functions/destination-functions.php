<?php

/*
 * Destination Functions
 */


class Destinations {

	public function __construct() {
		
	}

	//List of destinations
	public function view_destination_list($showposts) {
		global $post;

		
	}

	public function get_destination_arg($showposts=1) {

	    if(isset($_GET['dscountry']) && isset($_GET['dsstate']) && isset($_GET['dscity'])) {
	        $country_id = (int) mysql_real_escape_string(trim($_GET['dscountry']));
	        $state_id = mysql_real_escape_string(trim($_GET['dsstate']));
	        $city_id = (int) mysql_real_escape_string(trim($_GET['dscity']));

	        $args = array(
	            'showposts' => $showposts, 
	            'post_type' => 'destinations',
	            'meta_query'=> array(
	                array(
	                    'key'       => 'destinationlocation2',
	                    'value'     => ':"'.$country_id.'";',
	                    'compare'   => 'LIKE'
	                ),
	                array(
	                    'key'       => 'destinationlocation2',
	                    'value'     => ':"'.$city_id.'";',
	                    'compare'   => 'LIKE'
	                ),
	                array(
	                    'key'       => 'destinationlocation2',
	                    'value'     => ':"'.$state_id.'";',
	                    'compare'   => 'LIKE'
	                ),
	            ),                               
	            'orderby'       => 'post_date',
	            'order'         => 'DESC',
	            'post_status'   => 'publish'
	        );
	    } else if(isset($_GET['shownewdestination']) && is_user_logged_in()) {
	        $args = get_destination_shownew_args($showposts);
	    } else {
	        $args = array(
	            'showposts'     => $showposts, 
	            'post_type'     => 'destinations',                            
	            'orderby'       => 'post_date',
	            'order'         => 'DESC',
	            'post_status'   => 'publish'
	        );
	    }

    	return $args;
	}



	public function get_destination_shownew_args($showposts=1, $updatemeta=true) {
    
	    $lastview = array();
	    $current_user = wp_get_current_user();
	    $usermeta = get_user_meta($current_user->ID, 'show-new-destination-lastview', TRUE);
	    if ($usermeta) {
	        $lastview = unserialize($usermeta);
	        if (count($lastview)==1) {
	            $after = explode('-', $lastview[0]);
	            if ($lastview[0] != date('Y-m-d')) {
	                $lastview[] = date('Y-m-d');
	                $usermeta = serialize($lastview);
	            }
	        } else if ($lastview[1] == date('Y-m-d')) {
	            $after = explode('-', $lastview[0]);
	        } else {
	            $after = explode('-', $lastview[1]);
	            $usermeta = serialize(array($lastview[1], date('Y-m-d')));
	        }
	    } else {
	        list($user_reg_date, $user_reg_time) = explode(' ',$current_user->user_registered);
	        $after = explode('-', $user_reg_date);
	        $usermeta = serialize(array($user_reg_date));
	    }
	    $before = explode('-',date('Y-m-d',strtotime(date('Y-m-d') . '+1 day')));
	    if($updatemeta){ update_user_meta($current_user->ID, 'show-new-destination-lastview', $usermeta); }

	    $args = array(
	        'date_query' => array(
	            array(
	                'after'     => array(
	                    'year'  => $after[0],
	                    'month' => $after[1],
	                    'day'   => $after[2],
	                ),
	                'before'    => array(
	                    'year'  => $before[0],
	                    'month' => $before[1],
	                    'day'   => $before[2],
	                ),
	                'inclusive' => true,
	            ),
	        ),
	        'showposts'     => $showposts, 
	        'post_type'     => 'destinations',                            
	        'orderby'       => 'post_date',
	        'order'         => 'DESC',
	        'post_status'   => 'publish'
	    );

	    return $args;
	}



	public function destination_view_agents_modal() {
	    global $post;
	    ?>
	        <div id="viewDestinationAgentModal" class="modal fade">
	            <div class="modal-dialog">
	                <div class="modal-content boxtype">
	                    <div class="modal-header noborders">
	                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                        <h4 class="modal-title mypopuptitle" id="myModalLabel">List of Agents</h4>
	                        <h5 class="modal-sub-title">They are the agents who specialized in this destination</h5>
	                    </div>
	                    <div class="modal-body">
	                        <div id="contact-popup">
	                        <!-- Content Start -->
	                            
	                             <div id="experts-box">
	                            
	                                <?php
	                                    $agents = get_field('destinationagents');
	                                    if($agents) {
	                                        foreach($agents as $agent) {
	                                            $agent_id = $agent['ID'];
	                                            $agent_name = $agent['display_name'];
	                                            $agent_phone = get_field('agentcontact','user_'.$agent_id);
	                                            $agent_pic = get_field('profile_picture','user_'.$agent_id);
	                                            $agent_position = get_field('agentposition','user_'.$agent_id);

	                                ?>
	                                    <div class="item-expert">
	                                        <?php
	                                            if ( $agent_pic ) {
	                                        ?>
	                                            <a href="<?php echo site_url();?>/agents/?id=<?php echo $agent_id;?>">
	                                                <img src="<?php echo $agent_pic['sizes']['thumbnail']; ?>" class="expert-img" style="width: 74px; height: 74p;" />
	                                            </a>
	                                        <?php
	                                            }else{
	                                        ?>
	                                            <a href="<?php echo site_url();?>/agents/?id=<?php echo $agent_id;?>">
	                                                <img src="<?php bloginfo('template_directory');?>/img/default_avatar.jpg" class="expert-img" style="width: 74px; height: 74px;" />
	                                            </a>
	                                         <?php
	                                            }
	                                        ?>
	                                        <div class="expert-desc">
	                                            <h3><a href="<?php echo site_url();?>/agents/?id=<?php echo $agent_id;?>"><?php echo $agent_name; ?></h3></a>
	                                            <?php echo $agent_position; ?>
	                                            <?php echo $agent_phone; ?>
	                                            <?php //the_field('agentdesignations','user_'.$agent_id);?>
	                                            <br />
	                                            <a href="<?php echo site_url();?>/agents/?id=<?php echo $agent_id;?>">View more details »</a>
	                                        </div>
	                                        
	                                        <div class="clearthis"></div>
	                                    
	                                    </div>

	                                <?php
	                                        }
	                                    } else {
	                                        echo "No experts found.";
	                                    }
	                                ?>
	                                
	                                
	                            </div>

	                        <!-- Content End -->
	                            </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    <?php
	}


	public function destination_modal() {
		?>
		<!-- destinations modal -->
		<div id="myModald" class="modal fade">
		    <div class="modal-dialog">
		        <div class="modal-content boxtype">
		            <div class="modal-header noborders">
		                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                <h4 class="modal-title mypopuptitle" id="myModalLabel">Destinations Search</h4>
		                <h5 class="modal-sub-title">Please complete the following to start your search:</h5>
		            </div>
		            <div class="modal-body">
		                <div id="contact-popup">
		                    <form action="<?php bloginfo('url') ?>/golf-destinations/" method="GET">
		                        <div class="cdsel" id="dscountryc">
		                            <div class="left-col">
		                                <p><label>Country</label></p>
		                            </div>
		                            <div class="right-col">
		                                <select id="dscountry" name="dscountry" class="psst">
		                                    <option value="">Select Country</option>
		                                    <?php 

		                                        $countries = rem_get_countries(); 
		                                        foreach($countries as $country => $val ) {
		                                    ?>
		                                        <option value="<?php echo $country; ?>"><?php echo $val; ?></option>
		                                    <?php
		                                        }

		                                    ?>
		                                    <!-- <option value="us">United States</option>
		                                    <option value="ca">Canada</option> -->

		                                    <!-- DISPLAY ALL COUNTRY ON EVERY DESTINATION -->
		                                    
		                                </select>
		                            </div>
		                            <div class="clearthis"></div>
		                        </div>
		                        <div class="clearthis"></div>

		                        <div class="cdsel" id="dsstatec">
		                            <div class="left-col">
		                                <p><label>State</label></p>
		                            </div>
		                            <div class="right-col">
		                                <select id="dsstate" name="dsstate" class="psst">
		                                    <option value="">Select State</option>
		                                </select>                            
		                            </div>
		                            <div class="clearthis"></div>
		                        </div>
		                        <div class="clearthis"></div>

		                        <div class="cdsel" id="dscityc">
		                            <div class="left-col">
		                                <p><label>City</label></p>
		                            </div>
		                            <div class="right-col">
		                                <select id="dscity" name="dscity" class="psst">
		                                    <option value="">Select City</option>
		                                    <option value="la">L.A.</option>
		                                    <option value="sf">San Francisco</option>
		                                </select>                           
		                            </div>
		                            <div class="clearthis"></div>
		                        </div>
		                        <div class="clearthis"></div>
		                        
		                        <div class="" id="dsboxesc">
		                            <div class="left-col">
		                                <p><label> </label></p>
		                            </div>
		                            <div class="gdtb right-col">                                
		                                <div class="left-col">
		                                    <input type="submit" value="Submit →" class="wpcf7-form-control wpcf7-submit s-des btn fleft">
		                                </div>
		                                <div class="clearthis"></div> 
		                            </div>
		                            <div class="clearthis"></div>
		                        </div>
		                        <div class="clearthis"></div>
		                    </form>
		                    <div class="clearthis"></div>
		                </div>
		            </div>
		        </div>
		    </div>  
		</div>
		<!-- end modal destinations search -->
		<?php
	}

}

$destination_class = new Destinations();



