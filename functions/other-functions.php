<?php

/*
 * Other Important Functions
 */


function get_answers_community_modal($email_array) {
		global $post;
	?>

    <div id="getAnswersCommunityModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content boxtype">
                <div class="modal-header noborders">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title mypopuptitle" id="myModalLabel">Get Answers</h4>
                    <h5 class="modal-sub-title">Ask about this community. Communicate through email.</h5>
                </div>
                <div class="modal-body">
                    <div id="contact-popup">
                    	<!-- Content Start -->


                    	<?php 

							if($_POST['gasubmit']):
								    //$caname = trim($_POST['caname']);
								    $ganame = trim($_POST['ganame']);
								    $gaemail = trim($_POST['gaemail']);
								    $gaphone = trim($_POST['gaphone']);
								    $gamessage = trim($_POST['gamessage']);
								    $gasubject = trim($_POST['gasubject']);

								    //$to = '';
								    $message = '';
								    $message .= '<p>Name: '.$ganame.'</p>';
								    $message .= '<p>Email: '.$gaemail.'</p>';
								    $message .= '<p>Phone: '.$gaphone.'</p>';
								    $message .= '<p>Message: '.$gamessage.'</p>';

								    $headers = "From: $ganame <$gaemail>";

								    add_filter( 'wp_mail_content_type', 'set_content_type' );
								    function set_content_type( $content_type ){
								        return 'text/html';
								    }
								    if(wp_mail( $email_array, $gasubject, $message, $headers )) {
								    	echo "<h4>Message sent.</h4>";
								    } else {
								    	echo "<h4>Something went wrong, please try again.</h4>";
								    }
	                    	

	                    	else: 
	                    ?>                    	
                    		
							<label>Recepients: all agents of this community</label>
							<br/>
							<?php //print_r($email_array); ?>
							<form action="<?php echo "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>" method="post">

	                        <div class="cdsel2" id="cnamec2">
	                            <div class="left-col">
	                                <p><label>Your Name</label></p>
	                            </div>
	                            <div class="right-col">
	                                <input type="text" name="ganame" id="ganame" required>                        
	                            </div>
	                            <div class="clearthis"></div>

	                             <div class="left-col">
	                                <p><label>Your Email</label></p>
	                            </div>
	                            <div class="right-col">
	                                <input type="email" name="gaemail" id="gaemail" required>                        
	                            </div>
	                            <div class="clearthis"></div>

	                            <div class="left-col">
	                                <p><label>Your Phone #</label></p>
	                            </div>
	                            <div class="right-col">
	                                <input type="text" name="gaphone" id="gaphone" required>                        
	                            </div>
	                            <div class="clearthis"></div>

	                            <div class="left-col">
	                                <p><label>Subject </label></p>
	                            </div>
	                            <div class="right-col">
	                                <input type="text" name="gasubject" id="gasubject" required>                        
	                            </div>
	                            <div class="clearthis"></div>

	                            <div class="left-col">
	                                <p><label>Message</label></p>
	                            </div>
	                            <div class="right-col">
	                                <textarea name="gamessage" id="gamessage"></textarea>              
	                            </div>
	                            <div class="clearthis"></div>
	                        
	                            <div class="left-col">
	                                <p><label> </label></p>
	                            </div>
	                            <div class="gdtb right-col">
	                               
	                                <div class="left-col">
	                                    <input type="submit" name="gasubmit" value="Submit →" class="wpcf7-form-control wpcf7-submit s-des btn fleft">
	                                </div>
	                                <div class="clearthis"></div> 
	                            </div>
	                            <div class="clearthis"></div>
	                        </div>

	                        <div class="clearthis"></div>
	                    </form>
                	<?php endif; ?>



                    <div class="clearthis"></div>

                    <!-- Content End -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
}



/**
    Get Community data by Agent ID
    returns array
*/
function get_community_by_listing_key($id)
{
    $community = array();
    $args = array(
        'post_type' => 'communities',
        'meta_query' => array(
                array(
                    'key' => '_comdest_property_value_key',
                    'value' => ':"'.$id.'";',
                    'compare' => 'LIKE'
                )
            ),  
    );
    $the_query = new WP_Query($args);

    while($the_query->have_posts()) {
        $the_query->the_post();

        	$community = $the_query->posts[0];
        break;
    }
    wp_reset_query();
    
    return $community;
}