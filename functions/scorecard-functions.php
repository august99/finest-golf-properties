<?php 

/**
 ** Scorecard Functions
 **/

class Scorecard {

	public function __construct() {

	}

	public function display_favorite_comdest_list($posttype,$favorite_post_ids) {

		?>
		<div class="sr-headline row">
			<?php if($posttype == 'communities'): ?>
				<h2>My Scorecard - Communities</h2>
			<?php elseif($posttype == 'destinations'): ?>
				<h2>My Scorecard - Destinations</h2>
			<?php endif; ?>
			<div class="sub-head"></div>

		</div>

		<div id="search-results-box">	    
	    	<div id="search-content">
				<?php


				$ctr = 0;
				if ($favorite_post_ids) :

		            $favorite_post_ids = array_reverse($favorite_post_ids);
		            $qry = array('post__in' => $favorite_post_ids, 'orderby' => 'post__in');
		            $qry['post_type'] = array($posttype);
		            $post_in = count($qry['post__in']);

		            query_posts($qry);

		            if(($post_in != 0)) :
		                while ( have_posts() ) : $ctr++; the_post(); $picture = wp_get_attachment_image_src( get_post_thumbnail_id(), 'scorecard-com-thumb' );
							$the_location = get_post_meta(get_the_ID(), 'location_2');


							if($posttype == 'communities'):
								?>
									<div class="search-item">
										<?php if ( has_post_thumbnail() ) { ?>
					                        <div class="comdest-search-thumb">
					                     		<?php the_post_thumbnail('small'); ?>
					                        </div>
					                    <?php } else { ?>
					                        <div class="comdest-search-thumb">
					                            <img src="<?php bloginfo('template_directory');?>/img/img_not_available.jpg" class="img-responsive" />
					                        </div>
					                    <?php } ?>
											<h4><a href="<?php bloginfo('url'); ?>/my-scorecard/?type=communities&id=<?php echo get_the_ID(); ?>"><?php the_title(); ?></a></h4>
									        <div class="sr-desc">
									             <?php 
									                $desc=get_field('communitydescription');
									                $position = stripos ($desc, "."); //find first dot position

									                if($position) { //if there's a dot in our soruce text do
									                    $offset = $position + 1; //prepare offset
									                    $position2 = stripos ($desc, ".", $offset); //find second dot using offset
									                    $first_two = substr($desc, 0, $position2); //put two first sentences under $first_two

									                    echo $first_two . '.'; //add a dot
									                }

									                else {  //if there are no dots
									                    //do nothing
									                }
									            ?> 
									        </div>
								    </div>
								<?php
							elseif($posttype == 'destinations'):
								?>
									<div class="search-item">
										<?php if ( has_post_thumbnail() ) { ?>
					                        <div class="comdest-search-thumb">
					                     		<?php the_post_thumbnail('small'); ?>
					                        </div>
					                    <?php } else { ?>
					                        <div class="comdest-search-thumb">
					                            <img src="<?php bloginfo('template_directory');?>/img/img_not_available.jpg" class="img-responsive" />
					                        </div>
					                    <?php } ?>
											<h4><a href="<?php bloginfo('url'); ?>/my-scorecard/?type=destinations&id=<?php echo get_the_ID(); ?>"><?php the_title(); ?></a></h4>
									        <div class="sr-desc">
									             <?php 
									                $desc=get_field('destinationdescription');
									                $position = stripos ($desc, "."); //find first dot position

									                if($position) { //if there's a dot in our soruce text do
									                    $offset = $position + 1; //prepare offset
									                    $position2 = stripos ($desc, ".", $offset); //find second dot using offset
									                    $first_two = substr($desc, 0, $position2); //put two first sentences under $first_two

									                    echo $first_two . '.'; //add a dot
									                }

									                else {  //if there are no dots
									                    //do nothing
									                }
									            ?> 
									        </div>
								    </div>
								<?php
							endif;



						endwhile;
					endif;

					if($ctr == 0) :
						if($posttype == 'communities'):
		                	echo "<p class='no_item_message center'>No communities found.</p>";
		               	elseif($posttype == 'destinations'):
		               		echo "<p class='no_item_message center'>No destinations found.</p>";
		               	endif;
		            endif;

		            wp_reset_postdata();
			        wp_reset_query();

			    else :
			    	if($posttype == 'communities'):
		            	echo "<p class='no_item_message center'>No communities found.</p>";
		           	elseif($posttype == 'destinations'):
		           		echo "<p class='no_item_message center'>No destinations found.</p>";
		           	endif;
				endif;
				echo "<p id='desti_count_hidden' style='display: none;'>".$ctr."</p>";
				?>

				<span style="display: none;" class="search-count2"><?php echo $ctr; ?> matches</span>
		        <script>
		            jQuery(document).ready(function($) {
		                var count = $('.search-count2').html();
		                $('.sr-headline .sub-head').html(count);
		            });
		        </script>
		    </div>
		</div>

		       	<?php
	}


	public function display_favorite_agent_list($posttype, $favorite_agent_ids) {
		?>
		<div class="sr-headline row">
			<h2>My Scorecard - Agents</h2>
				<?php
					$matches=0;

                    $favorite_agent_ids = array_reverse($favorite_agent_ids);

                    $args = array(
                        'role' => 'aamrole_53ec21a063e46',
                        'include' => $favorite_agent_ids
                    );

                    $the_query= new WP_User_Query( $args);
                    //var_dump($the_query);
                    
                    if( ! empty( $the_query->results ) && (!empty($favorite_agent_ids)) ){
                        $matches= $the_query->get_total();
                    }
                ?>
			<div class="sub-head"><?php echo $matches;?> matches</div>
		</div>

		<div id="search-results-box">	    
	    	<div id="search-content">
				<?php                        
                	$pstc=1;
                    if( ! empty( $the_query->results ) && (!empty($favorite_agent_ids)) ) :
                   
	                    foreach ( $the_query->results as $user ) :
	                        $agent_id = $user->ID;
	                        $agent_email = get_the_author_meta('user_email',$agent_id); 
	                        $profile_pic = get_field('profile_picture','user_'.$agent_id);
	            ?>
							
							<div class="search-item-b">
								<?php if($profile_pic): ?>
									<?php echo wp_get_attachment_image( $profile_pic['id'], $size = 'thumbnail'); ?>
								<?php else: ?>
									<img src="<?php bloginfo('template_directory');?>/img/default_avatar.jpg" class="s-property-thumb img-responsive" />
								<?php endif; ?>
								<div class="s-property-desc">
	                                <p><div class="s-property-title"><?php echo $user->display_name; ?></div></p>
	                                <p><?php the_field('agentcontact','user_'.$agent_id); ?></p>
	                                <p><?php the_field('agentbrokerage','user_'.$agent_id);?></p>
	                                <p><a href="mailto:<?php echo $agent_email; ?>">Email Agent</a></p>
	                                <?php if(($country_id != "") || ($state_id != "") || ($city_id != "")) {  ?>
	                                <p><a href="<?php echo site_url();?>/agents/?id=<?php echo $agent_id;?>&acountry=<?php echo $country_id;?>&astate=<?php echo $state_id; ?>&acity=<?php echo $city_id; ?>">View Details »</a></p>
	                                <?php } else { ?>
	                                <p><a href="<?php bloginfo('url'); ?>/my-scorecard/?type=agents&id=<?php echo $agent_id;?>">View Details »</a></p>
	                                <?php } ?>
		                        </div>
		                        <div class="clearthis"></div>
							</div>

				<?php 
						endforeach;
					else:
						echo "<p class='center'>No Agents found in your scorecard.</p>";
					endif;
				?>
		    </div>
		</div>

		       	<?php
	}




	public function display_favorite_properties_list($type, $favorite_properties) {
		?>
	        <div class="sr-headline row">
	            <h2>My Scorecard - Properties</h2>
	        <div class="sub-head"><?php echo $favorite_properties?count($favorite_properties):0 ?> matches</div>
	        </div>
	       
	        <div id="search-results-box">
	            <div id="search-results-orderede" style="overflow:scroll">
	                  <?php if($favorite_properties):
	                    list($property, $price) = property_search('admin');
	                    foreach($favorite_properties as $key):
	                        $pImg = (string)(is_array($property[$key]->Photos->Photo)?$property[$key]->Photos->Photo[0]->MediaURL:$property[$key]->Photos->Photo->MediaURL);
	                        $agent = get_agent_by_listingkey(((string)$property[$key]->ListingKey));
	                        $photo = is_array($property[$key]->Photos->Photo)?$property[$key]->Photos->Photo[0]->MediaURL:$property[$key]->Photos->Photo->MediaURL;
	                    ?>
	                    <!--- search item -->
	                    <div class="search-item-b" onclick="javascript: window.location = '<?php bloginfo('url'); ?>/my-scorecard/?type=properties&listingkey=<?php echo $key; ?>';" listing-key="<?php echo $key ?>">
	                        <div class="search-item-img property-search-thumb">
	                            <img width="100" height="75" src="<?php echo (string)$photo ?>" class="s-property-thumb img-responsive" alt="<?php echo ((string)$property[$key]->ProviderName).' - '.((string)$property[$key]->PropertyType) ?>" />
	                        </div>
	                        <div class="s-property-desc">
	                            <p><div class="s-property-title">$ <?php echo number_format((string)$property[$key]->ListPrice) ?></div></p>
	                            <p><?php echo number_format((string)$property[$key]->Bedrooms) ?> Beds</p>
	                            <p><?php echo number_format((string)$property[$key]->FullBathrooms) ?> Full Baths</p>
	                            <p><?php echo number_format((string)$property[$key]->PartialBathrooms) ?> Part Baths</p>
	                            <!-- <p>11,758 SqFt</p> -->
	                        </div>
	                        <div class="clearthis"></div>
	                    </div>
	                    <!-- end search item -->
	                    <?php endforeach; ?>
	                <?php else: echo "<p class='center'>No properties found on your scorecard.</p>"; ?>
	                <?php endif; ?>

	            </div>
	        </div>
	    <?php
	}



	public function display_favorite_comdest_main_content_with_id($type, $favorite_post_ids) {
		$id = mysql_real_escape_string(trim($_GET['id']));

		$args = array(
    		'p' => $id,
    		'post_type' => $type,
    		'post_status' => 'publish'
    	);

    	$the_query = new WP_Query( $args );

    	if($the_query->have_posts()) :
    		while($the_query->have_posts()) :
    			$the_query->the_post();

    			if($type == 'communities'):
    				?>
						<div id="mid-col-main">
					        <div class="featured-image">
					            <div class="tab-content">
					                <div class="tab-pane active" id="community">
					                     <?php get_template_part('includes/tab/community','details-tab-content'); ?>
					                </div> 
					                <!-- end community tab -->

					                <div class="tab-pane" id="photos">
					                    <?php get_template_part('includes/tab/community','photos-tab-content' ); ?>
					                </div>
					                <!-- end photos tab -->

					                <div class="tab-pane" id="location">
					                    <?php get_template_part('includes/tab/community','location-tab-content' ); ?>
					                </div>
					                <!--end location tab -->

					                <div class="tab-pane" id="video">
					                    <?php get_template_part('includes/tab/community','video-tab-content' ); ?>
					                </div>
					                <!-- end vides tab -->
					            </div> <!-- end tab-content -->                        
					        </div><!-- END featured image -->
					    </div><!-- END MAIN -->
    				<?php
    			elseif($type == 'destinations'):
    				?>
						<div id="mid-col-main">
					        <div class="featured-image">
					            <div class="tab-content">
					                <div class="tab-pane active" id="community">
					                     <?php get_template_part('includes/tab/destination','details-tab-content'); ?>
					                </div> 
					                <!-- end community tab -->

					                <div class="tab-pane" id="photos">
					                    <?php get_template_part('includes/tab/destination','photos-tab-content' ); ?>
					                </div>
					                <!-- end photos tab -->

					                <div class="tab-pane" id="location">
					                    <?php get_template_part('includes/tab/destination','location-tab-content' ); ?>
					                </div>
					                <!--end location tab -->

					                <div class="tab-pane" id="video">
					                    <?php get_template_part('includes/tab/destination','video-tab-content' ); ?>
					                </div>
					                <!-- end vides tab -->
					            </div> <!-- end tab-content -->                        
					        </div><!-- END featured image -->
					    </div><!-- END MAIN -->
    				<?php
    			endif;
    		endwhile;
    	else:
    		echo "";
    	endif;

    	wp_reset_query();
	}



	public function display_favorite_comdest_main_content($type, $favorite_post_ids) {
		$ctr = 0;
        if ($favorite_post_ids) { 
            //var_dump($favorite_post_ids);
            $favorite_post_ids = array_reverse($favorite_post_ids);

            $qry = array('post__in' => $favorite_post_ids, 'orderby' => 'post__in');
            // custom post type support can easily be added with a line of code like below.
            $qry['post_type'] = array($type);
            $qry['posts_per_page'] = 1; 
            $post_in = count($qry['post__in']);
            query_posts($qry);

        if(($post_in != 0)) {
            while ( have_posts() ) : $ctr++; the_post(); $picture = wp_get_attachment_image_src( get_post_thumbnail_id(), 'scorecard-com-thumb' );
				 $the_location = get_post_meta(get_the_ID(), 'location_2');

                 if($type == 'communities'):
    				?>
						<div id="mid-col-main">
					        <div class="featured-image">
					            <div class="tab-content">
					                <div class="tab-pane active" id="community">
					                     <?php get_template_part('includes/tab/community','details-tab-content'); ?>
					                </div> 
					                <!-- end community tab -->

					                <div class="tab-pane" id="photos">
					                    <?php get_template_part('includes/tab/community','photos-tab-content' ); ?>
					                </div>
					                <!-- end photos tab -->

					                <div class="tab-pane" id="location">
					                    <?php get_template_part('includes/tab/community','location-tab-content' ); ?>
					                </div>
					                <!--end location tab -->

					                <div class="tab-pane" id="video">
					                    <?php get_template_part('includes/tab/community','video-tab-content' ); ?>
					                </div>
					                <!-- end vides tab -->
					            </div> <!-- end tab-content -->                        
					        </div><!-- END featured image -->
					    </div><!-- END MAIN -->
    				<?php
    			elseif($type == 'destinations'):
    				?>
						<div id="mid-col-main">
					        <div class="featured-image">
					            <div class="tab-content">
					                <div class="tab-pane active" id="community">
					                     <?php get_template_part('includes/tab/destination','details-tab-content'); ?>
					                </div> 
					                <!-- end community tab -->

					                <div class="tab-pane" id="photos">
					                    <?php get_template_part('includes/tab/destination','photos-tab-content' ); ?>
					                </div>
					                <!-- end photos tab -->

					                <div class="tab-pane" id="location">
					                    <?php get_template_part('includes/tab/destination','location-tab-content' ); ?>
					                </div>
					                <!--end location tab -->

					                <div class="tab-pane" id="video">
					                    <?php get_template_part('includes/tab/destination','video-tab-content' ); ?>
					                </div>
					                <!-- end vides tab -->
					            </div> <!-- end tab-content -->                        
					        </div><!-- END featured image -->
					    </div><!-- END MAIN -->
    				<?php
    			endif;

			endwhile;  
                
        } 
        

        if($ctr == 0) :
			if($type == 'communities'):
            	echo "<p class='no_item_message center'>No communities found.</p>";
           	elseif($posttype == 'destinations'):
           		echo "<p class='no_item_message center'>No destinations found.</p>";
           	endif;
        endif;

        	wp_reset_postdata();
        	wp_reset_query();

        } else {
            if($type == 'communities'):
           		echo "<p class='no_item_message center'>No communities found.</p>";
            elseif($type == 'destinations'):
           		echo "<p class='no_item_message center'>No destinations found.</p>";
            endif;
        }
        echo "<span id='desti_count_hidden' style='display: none;'>".$ctr."</span>";

	}


	public function agent_main_content($agent_id) {
		?>
			<div class="featured-image">

                <div class="profile-box">
                    <div class="apic">
                        <?php
                            $profile_pic = get_field('profile_picture','user_'.$agent_id);
                            $size = 'full';

                            if ( $profile_pic ) {
                                 echo "<img id='c_agentpicture' src='".$profile_pic['sizes']['thumbnail']."' style='width: 150px;height: 150px;' class='s-property-thumb img-responsive'/>";
                            } else {
                        ?>
                            <img id="c_agentpicture" src="<?php bloginfo('template_directory');?>/img/default_avatar.jpg" alt="agent" title="agent" class="agentpic" />
                        <?php
                            }
                        ?>
                    </div>
                    <!-- <img src="<?php bloginfo('template_directory');?>/img/karen-baldwin-profile-pic.jpg" alt="agent" title="agent" class="agentpic" /> -->
                    
                    <div class="profile-description">
                        <h3 id="c_agentname" class="agent-name"><?php echo get_the_author_meta('display_name',$agent_id); ?></h3>
                        <div id="c_agentposition" ><?php the_field('agentposition','user_'.$agent_id);?>, <?php the_field('agentdesignations','user_'.$agent_id);?></div>
                        <div ><?php the_field('agentcontact','user_'.$agent_id);?> | <a href="mailto:<?php echo get_the_author_meta('user_email', $agent_id); ?>">Email Me</a></div>
                        <br />
                        <div><?php the_field('agentbrokerage','user_'.$agent_id);?></div>
                        <div><?php the_field('agentbrokerageaddress','user_'.$agent_id);?></div>
                        <div id="c_agentbrokeragecity"><?php the_field('agentbrokeragecity','user_'.$agent_id);?>, <span id="c_agentbrokeragestate"><?php the_field('agentbrokeragestate','user_'.$agent_id);?></span> <?php the_field('agentbrokeragezipcode','user_'.$agent_id);?></div>
                        <br />
                        <div class="special-exp">Years Experience:   <span class="blackthis"><?php the_field('agentexperience','user_'.$agent_id);?></span></div>
                        <div class="special-exp">Community Specialist:</div>
                         <?php

                            $view_agent = get_the_author_meta('display_name',$agent_id);
                            $args = array(
                                'showposts' => -1, 
                                'post_type' => 'communities',                            
                                'orderby'          => 'post_date',
                                'order'            => 'DESC',
                                'post_status'      => 'publish'
                            );
                                
                                $the_query= new WP_query( $args);
                                //echo "before if";
                               //print_r($the_query);
                                $pstc=1;
                                //echo count($posts);
                                 if($the_query->have_posts()){
                                    while ( $the_query->have_posts() ) {
                                        $the_query->the_post();


                                        $agents = get_field('communityagents');
                                        if($agents) {
                                            foreach($agents as $agent) {
                                                $agent_name = $agent['display_name'];

                                                if($agent_name == $view_agent) {
                                    ?>
                                    <div>- <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div> 

                                    <?php
                                                }
                                            }
                                        }                                                   
                                    }

                                    wp_reset_postdata();
                                    wp_reset_query();
                                 }
                        ?>

                    </div>
                    
                    <div class="clearthis"></div>
                    
                    <div class="biography-box">
                        <h4 class="bio">Biography</h4>
                        <?php the_field('agentbio','user_'.$agent_id);?>

                    </div>
                </div>
            </div>
		<?php
	}




	public function display_favorite_agent_main_content($type, $favorite_agent_ids) {
    ?>
		<div id="mid-col-main">
		    <?php
			        if( isset($_GET['id']) && is_numeric($_GET['id']) ) {

			            $agent_id = mysql_real_escape_string(trim($_GET['id']));
			            $aux = get_userdata( $agent_id );

			            if($aux==false) {
			                echo "<h3>User does not exists.</h3>";
			            } else {
			            	$this->agent_main_content($agent_id);
		            	}
		        	} else {
		        		$favorite_agent_ids = array_reverse($favorite_agent_ids);

                        $args = array(
                            'role' => 'aamrole_53ec21a063e46',
                            'include' => $favorite_agent_ids,
                            'number' => 1
                        );

	                    $agent_query2= new WP_User_Query( $args );

	                    if( ! empty( $agent_query2->results ) && (!empty($favorite_agent_ids)) ) :

	                        foreach($agent_query2->results as $user2) :
	                            $agent_email = get_the_author_meta('user_email',$user2->ID); 
	                        	$agent_id = $user2->ID;

	                        	$this->agent_main_content($agent_id);

	                        endforeach;
	                    endif;
		        	}
			?>

		</div> <!-- Mid col Main -->
	<?php
	}



	public function display_favorite_agent_right_content($type, $favorite_agent_ids) {
    ?>
		<div id="mid-col-main">
		    <?php
			        if( isset($_GET['id']) && is_numeric($_GET['id']) ) {

			            $agent_id = mysql_real_escape_string(trim($_GET['id']));
			            $agent_email = get_the_author_meta('user_email',$_GET['id']); 
			            $aux = get_userdata( $agent_id );

			            if($aux==false) {
			                echo "<h3>User does not exists.</h3>";
			            } else {
			            	$this->agent_right_content($agent_id, $agent_email);
		            	}
		        	} else {
		        		$favorite_agent_ids = array_reverse($favorite_agent_ids);

                        $args = array(
                            'role' => 'aamrole_53ec21a063e46',
                            'include' => $favorite_agent_ids,
                            'number' => 1
                        );

	                    $agent_query2= new WP_User_Query( $args );

	                    if( ! empty( $agent_query2->results ) && (!empty($favorite_agent_ids)) ) :

	                        foreach($agent_query2->results as $user2) :
	                            $agent_email = get_the_author_meta('user_email',$user2->ID); 
	                        	$agent_id = $user2->ID;

	                        	$this->agent_right_content($agent_id, $agent_email);

	                        endforeach;
	                    endif;
		        	}
			?>

		</div> <!-- Mid col Main -->
	<?php
	}




	public function agent_right_content($agent_id, $agent_email) {
		?>
			<div id="sidebar-r" class="row">
				<div class="item-box">
               		<a href="#"><img src="<?php bloginfo('template_directory');?>/img/contact-icon.png" class="sidebar-r-ico" /> </a>
                	<div class="sidebar-r-text">
                 		<a href="mailto:<?php echo $agent_email; ?>">Contact Agent</a>
                	</div>
                	<div class="clearthis"></div>
                </div>
                
                <div class="item-box">
                	<a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon1.png" class="sidebar-r-ico" /></a>
                	<div class="sidebar-r-text">
                		<a href="<?php bloginfo('url'); ?>/property-search/?agentid=<?php echo $agent_id; ?>&agent-show-property=true">See All My Listings</a>
                	</div>
                	<div class="clearthis"></div>
                </div>
                
                
                <div class="item-box">
                	<a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon3.png" class="sidebar-r-ico" /></a>
                	<div class="sidebar-r-text">
                		<a href="#" class="get-answer-link" data-toggle="modal" data-target="#getAnswersCommunityModal">Get Answers</a>
                		<?php if($_POST['gasubmit']): ?>
                            <script>
                                jQuery(document).ready(function() {
                                    jQuery('a.get-answer-link').trigger('click');
                                });
                            </script>
                		<?php endif; ?>
                	</div>
                	<div class="clearthis"></div>
                </div>
            

	            <!-- scorecard -->
	            
	            <?php if ( is_user_logged_in() ) { ?>

		            <div class="scorecard">
		                <div class="scorecard-content">
		                    <?php wpfp_agent_link(0, "", 0, array( 'agent_id' => $agent_id )) ?>
		                </div>
		            </div>
	            
	            <?php } else { echo "<br/>";} ?>
            
            
		            <!-- more links -->
		            
		            <div class="more-links">
		                <a href="#" onclick="window.print();return false"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-print.png" alt="Print" class="ml-iconf" /></a>
		                <a href="mailto:<?php echo $agent_email; ?>"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-mail.png" alt="Mail" class="ml-icon" /></a>
		                <a href="#" data-toggle="modal" data-target="#shareModal"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-share.png" alt="Share" class="ml-icon" /></a>
		            </div>

		            <?php share_this_items( get_the_permalink(), get_the_title() ); ?>
					
					<?php get_answers_community_modal(array($agent_email)); ?>
	            
	            	<div class="e-agentbt"><a href="mailto:<?php echo $agent_email; ?>">Email Agent</a></div>
			</div>
		<?php
	}




	public function properties_main_content($type, $favorite_properties) {
		?>
		    <div id="mid-col-main">

		            <?php 
		            $ctr = 0;

		            if ($favorite_properties):

		                list($listing, $price) = property_search('admin');

		                foreach($favorite_properties as $key):
		                    $ctr++;

		                    if(isset($_GET['listingkey'])) {
		                        $mylisting = $listing[$_GET['listingkey']];
		                    } else {
		                        $mylisting = $listing[$key];
		                    }

		                    $pImg = (string)(is_array($mylisting->Photos->Photo)?$mylisting->Photos->Photo[0]->MediaURL:$mylisting->Photos->Photo->MediaURL);
		                    $agent = get_agent_by_listingkey(((string)$mylisting->ListingKey));
		                    $photo = is_array($mylisting->Photos->Photo)?$mylisting->Photos->Photo[0]->MediaURL:$mylisting->Photos->Photo->MediaURL;
		                    
		                    $propertyimg = (string)(is_array($mylisting->Photos->Photo)?$mylisting->Photos->Photo[0]->MediaURL:$mylisting->Photos->Photo->MediaURL);

		                    $agent = get_agent_by_listingkey(((string)$mylisting->ListingKey));

		                    $community = get_community_by_listing_key((string)$mylisting->ListingKey);
		                    $community_id = "#";
		                    $community_name = "Not Set";
		                    $community_link = '#';

		                    if(!empty($community)) {
		                        $community_id = $community->ID;
		                        $community_name = $community->post_title;
		                        $community_link = get_bloginfo('url').'?p='.$community->ID;
		                    } 

		                    if($mylisting) {
		                        $listing_address = $mylisting->Address->children('commons', true);
		                    }





		                    if( $ctr == 1):
		                ?>
		                

		                <div class="featured-image">
		                    
		                    <div class="tab-content">
		                        <!-- property-detail -->
		                        <div class="tab-pane active" id="property-detail">
		                            <div class="featured-content">
		                                <div class="s-property-img-big" style="background: url(<?php echo $photo ?>) no-repeat center center; background-size: cover;">
		                                    <!-- <img src="" title="big" class="img-responsive" /> -->
		                                </div>
		                                <div class="s-property-map">
		                                    <!--<img src="<?php bloginfo('template_directory');?>/img/sr-propertymap-big-img.jpg" title="map" class="img-responsive" />-->
		                                    <div class="acf-map">
		                                        <div class="marker img-responsive" data-lat="<?php echo (string)$mylisting->Location->Latitude; ?>" data-lng="<?php echo (string)$mylisting->Location->Longitude; ?>"></div>
		                                    </div>
		                                </div>
		                                <div class="clearthis"></div>
		                            </div>
		                            <div class="text-content">
		                                <h2 class="community-name"><?php echo (string)$mylisting->ListingTitle ?></h2>
		                                <h3 class="community-address"><?php echo $listing_address->FullStreetAddress ?>, <?php echo $listing_address->City ?>, <?php echo $listing_address->StateOrProvince ?>, <?php echo $listing_address->Country ?></h3>
		                                <div class="description-tbl noborders">
		                                    <span class="item-price">$ <?php echo number_format((string)$mylisting->ListPrice) ?> USD</span>
		                                    <select class="selectbox" id="convert-item-price">
		                                        <option value="USD">U.S. Dollars</option>     
		                                        <option value="SGD">S.G. Dollars</option>
		                                    </select>
		                                    <input type="hidden" id="current-convertion" value="USD"/>
		                                    <input type="hidden" id="current-price" value="<?php echo (string)$mylisting->ListPrice ?>"/>
		                                </div>
		                                <div class="property-detail-box">
		                                    <div class="sp-col-1">
		                                        <div class="lefty">Community:</div><div class="righty"><?php echo $community_name; ?></div> <div class="clearthis"></div>
		                                        <div class="lefty">Bedrooms: </div>     <div class="righty"><?php echo (string)$mylisting->Bedrooms ?></div> <div class="clearthis"></div>
		                                        <div class="lefty">Baths: </div>        <div class="righty"><?php echo (string)$mylisting->Bathrooms ?></div> <div class="clearthis"></div>
		                                        <div class="lefty">Partial Baths: </div>    <div class="righty"><?php echo (string)$mylisting->PartialBathrooms ?></div> <div class="clearthis"></div>
		                                        <!-- <div class="lefty">Square Footage: </div>   <div class="righty">11,758</div>  <div class="clearthis"></div> -->
		                                        <div class="lefty">Acres: </div>        <div class="righty"><?php echo (string)$mylisting->LotSize ?></div ><div class="clearthis"></div>
		                                    </div>
		                                    <div class="sp-col-2">
		                                        <div class="lefty">Agent: </div>        <div class="righty"><?php echo $agent['display_name'] ? $agent['display_name'] : 'Not Set' ?></div ><div class="clearthis"></div>
		                                        <div class="lefty">&nbsp;</div>     <div class="righty regularfont"><?php the_field('agentposition','user_'.$agent['ID']); ?></div ><div class="clearthis"></div>
		                                        <div class="lefty">&nbsp;</div>     <div class="righty regularfont"><?php the_field('agentcontact','user_'.$agent['ID']); ?></div ><div class="clearthis"></div>
		                                    </div>
		                                    <div class="clearthis"></div>
		                                </div>
		                                <div class="propery-full-desc">
		                                    <h3>Property Description:</h3>
		                                    <p><?php echo (string)$mylisting->ListingDescription ?></p>
		                                </div>                                    
		                            </div>
		                        </div>
		                        <!-- end property-detail tab -->

		                        <div class="tab-pane" id="photos">
		                            <h2 class="community-name"><?php echo (string)$mylisting->ListingTitle ?></h2>
		                                <h3 class="community-address"><?php echo $listing_address->FullStreetAddress ?>, <?php echo $listing_address->City ?>, <?php echo $listing_address->StateOrProvince ?>, <?php echo $listing_address->Country ?></h3>
		                            <div class="image-box">
		                                <div id="gallerymainimg">
		                                    <div class="picnt">

		                                    </div>
		                                </div>
		                                <script>
		                                    jQuery(document).ready( function($){
		                                        
		                                        var imgsrc=$("#wpsimplegallery_container ul li:first-child a").attr('href');

		                                        console.log(imgsrc);
		                                        $(".picnt").html('<img src="'+imgsrc+'" />');
		                                        $("#wpsimplegallery_container ul li a").click( function(e){

		                                            var newsrc=$(this).attr('href');

		                                            console.log(newsrc+" new src");
		                                            jQuery(".picnt").fadeOut('slow',function(){
		                                                jQuery(".picnt img").attr('src',newsrc);
		                                                jQuery(this).fadeIn('slow');
		                                               
		                                            });
		                                            e.preventDefault();
		                                        });
		                                    });
		                                </script>
		                                <style>
		                                #wpsimplegallery_container .clearfix {
		                                    zoom: 1;
		                                    font-size: 0;
		                                }
		                                #gallerymainimg img { max-height: none;}
		                                #mid-col-main .indv-thumbnails img, #mid-col-main .image-box img { box-shadow: none;}
		                                </style>
		                                <div id="wpsimplegallery_container">
		                                    <ul id="wpsimplegallery" class="clearfix">
		                                        <?php if(is_array($mylisting->Photos->Photo)): ?>
		                                        <?php foreach($mylisting->Photos->Photo as $ph): ?>
		                                        <li><a href="<?php echo (string)$ph->MediaURL ?>"><img src="<?php echo (string)$ph->MediaURL ?>" alt="<?php echo basename((string)$ph->MediaURL) ?>" rel="wpsimplegallery_group_184"/></a></li>
		                                        <?php endforeach; ?>
		                                        <?php else: ?>
		                                        <?php foreach($mylisting->Photos->Photo as $photos): ?>
		                                            <li><a href="<?php echo ((string)$photos->MediaURL); ?>"><img src="<?php echo ((string)$photos->MediaURL); ?>" alt="<?php echo basename((string)$photos->MediaURL); ?>" rel=""/></a></li>
		                                        <?php endforeach; ?>
		                                        
		                                        <?php endif; ?> 
		                                    </ul>
		                                </div>
		                                <div class="clearthis"></div>
		                            </div>
		                        </div>
		                        <!-- end photos tab -->

		                        <div class="tab-pane" id="location">
		                            <div class="text-content">
		                                <h2 class="community-name"><?php echo (string)$mylisting->ListingTitle ?></h2>
		                                <h3 class="community-address"><?php echo $listing_address->FullStreetAddress ?>, <?php echo $listing_address->City ?>, <?php echo $listing_address->StateOrProvince ?>, <?php echo $listing_address->Country ?></h3>
		                            </div>

		                            <div class="acf-map">
		                                <div class="marker" data-lat="<?php echo (string)$mylisting->Location->Latitude; ?>" data-lng="<?php echo (string)$mylisting->Location->Longitude; ?>"></div>
		                            </div>
		                        </div>
		                        <!-- end location tab -->

		                        <div class="tab-pane" id="agent">
		                            <div class="text-content">
		                                <h2 class="community-name"><?php echo (string)$mylisting->ListingTitle ?></h2>
		                                <h3 class="community-address"><?php echo $listing_address->FullStreetAddress ?>, <?php echo $listing_address->City ?>, <?php echo $listing_address->StateOrProvince ?>, <?php echo $listing_address->Country ?></h3>
		                            </div>

		                            <?php //print_r($agent) ?>
		                            <?php if($agent): ?>
		                            <div class="profile-box">
		                                <div class="apic">
		                                    <?php
		                                        $profile_pic = get_field('profile_picture','user_'.$agent['ID']);
		                                        $size = 'full';

		                                        if ( $profile_pic ) {
		                                             echo "<img id='c_agentpicture' src='".$profile_pic['sizes']['thumbnail']."' style='width: 150px;height: 150px;' class='s-property-thumb img-responsive'/>";
		                                        } else {
		                                    ?>
		                                        <img id="c_agentpicture" src="<?php bloginfo('template_directory');?>/img/default_avatar.jpg" alt="agent" title="agent" class="agentpic" />
		                                    <?php
		                                        }
		                                    ?>
		                                </div>

		                                <!-- <img src="http://50.87.50.55/~finestg1/wp-content/themes/golf/img/karen-baldwin-profile-pic.jpg" alt="agent" title="agent" class="agentpic"> -->
		                                <div class="profile-description">
		                                    <h3 class="agent-name"><?php echo $agent['display_name'] ?></h3>
		                                    <div><?php the_field('agentposition','user_'.$agent['ID']);?>, <?php the_field('agentdesignations','user_'.$agent['ID']);?></div>
		                                    <div><?php the_field('agentcontact','user_'.$agent['ID']);?> | <a href="mailto:<?php echo get_the_author_meta('user_email', $agent['ID']); ?>">Email Me</a></div>
		                                    <br>
		                                    <div><?php the_field('agentbrokerage','user_'.$agent['ID']);?></div>
		                                    <div><?php the_field('agentbrokerageaddress','user_'.$agent['ID']);?></div>
		                                    <div><?php the_field('agentbrokeragecity','user_'.$agent['ID']);?>, <span id="c_agentbrokeragestate"><?php the_field('agentbrokeragestate','user_'.$agent['ID']);?></span> <?php the_field('agentbrokeragezipcode','user_'.$agent['ID']);?></div>
		                                    <br>
		                                    <div class="special-exp">Years Experience:   <span class="blackthis"><?php the_field('agentexperience','user_'.$agent['ID']);?></span></div>
		                                    <div class="special-exp">Community Specialist:</div>
		                                    <?php

		                                        $view_agent = get_the_author_meta('display_name',$agent['ID']);
		                                        $args = array(
		                                            'showposts' => -1, 
		                                            'post_type' => 'communities',                            
		                                            'orderby'          => 'post_date',
		                                            'order'            => 'DESC',
		                                            'post_status'      => 'publish'
		                                        );
		                                            
		                                            $the_query= new WP_query( $args);
		                                            //echo "before if";
		                                           //print_r($the_query);
		                                            $pstc=1;
		                                            //echo count($posts);
		                                             if($the_query->have_posts()){
		                                                while ( $the_query->have_posts() ) {
		                                                    $the_query->the_post();


		                                                    $agents = get_field('communityagents');
		                                                    if($agents) {
		                                                        foreach($agents as $agent2) {
		                                                            $agent_name = $agent2['display_name'];

		                                                            if($agent_name == $view_agent) {
		                                                ?>
		                                                <div>- <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div> 

		                                                <?php
		                                                            }
		                                                        }
		                                                    }                                                   
		                                                }

		                                                wp_reset_query();
		                                                wp_reset_postdata();
		                                             }


		                                         
		                                    ?>
		                                </div>

		                                <div class="clearthis"></div>

		                                <div class="biography-box">
		                                    <h4 class="bio">Biography: </h4>
		                                    <?php the_field('agentbio','user_'.$agent['ID']);?>
		                                </div>
		                            </div>
		                            <?php endif; ?>
		                        </div>
		                        <!-- end agent tab -->

		                        <!--
		                        <div class="tab-pane" id="video">
		                            <?php //get_template_part('includes/tab/community','video-tab-content') ?>
		                        </div>
		                        -->
		                        <!-- end video tab -->

		                    </div>
		                </div>

		        <?php endif; ?>
		        <?php endforeach; ?>
		        <?php else: echo "No properties found on your scorecard."; ?>
		        <?php endif; ?>
		        <!-- END featuredimage -->
		    </div>
		<?php
	}




	public function property_right_content($type, $favorite_properties) {

        $ctr = 0;

        if ($favorite_properties):

            list($listing, $price) = property_search('admin');

            foreach($favorite_properties as $key):
                $ctr++;

                if(isset($_GET['listingkey'])) {
                    $mylisting = $listing[$_GET['listingkey']];
                } else {
                    $mylisting = $listing[$key];
                }

                $pImg = (string)(is_array($mylisting->Photos->Photo)?$mylisting->Photos->Photo[0]->MediaURL:$mylisting->Photos->Photo->MediaURL);
                $agent = get_agent_by_listingkey(((string)$mylisting->ListingKey));
                $photo = is_array($mylisting->Photos->Photo)?$mylisting->Photos->Photo[0]->MediaURL:$mylisting->Photos->Photo->MediaURL;
                
                $propertyimg = (string)(is_array($mylisting->Photos->Photo)?$mylisting->Photos->Photo[0]->MediaURL:$mylisting->Photos->Photo->MediaURL);

                $agent = get_agent_by_listingkey(((string)$mylisting->ListingKey));

                $community = get_community_by_listing_key((string)$mylisting->ListingKey);
                $community_id = "#";
                $community_name = "Not Set";
                $community_link = '#';

                if(!empty($community)) {
                    $community_id = $community->ID;
                    $community_name = $community->post_title;
                    $community_link = get_bloginfo('url').'?p='.$community->ID;
                } 

                if($mylisting) {
                    $listing_address = $mylisting->Address->children('commons', true);
                }





                if( $ctr == 1):
	                ?>


	                    <div id="sidebar-r">

	                        <?php if($agent): ?>       
		                        <div class="item-box">
		                        	<a href="#">img src="<?php bloginfo('template_directory');?>/img/contact-icon.png" class="sidebar-r-ico" /> </a>
		                        	<div class="sidebar-r-text">
		                         		<a href="mailto:<?php echo get_the_author_meta('user_email', $agent['ID']); ?>">Contact Agent</a>
		                        	</div>
		                            <div class="clearthis"></div>
		                        </div>
	                        <?php endif; ?>
	                        
	                        <?php if(isset($_GET['comunityid']) || ($community_id != '#')): ?>
		                        <div class="item-box">
		                        	<a href="#"><img src="<?php bloginfo('template_directory');?>/img/info-icon.png" class="sidebar-r-ico" /></a>
		                        	<div class="sidebar-r-text">
		                        		<a href="<?php echo $community_link; ?>">More About Community</a>
		                        	</div>
		                        	<div class="clearthis"></div>
		                        </div>
	                        <?php endif; ?>
	                        
	                        
		                        <div class="item-box">
		                        	<a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon3.png" class="sidebar-r-ico" /></a>
		                        	<div class="sidebar-r-text">
		                        		<a href="#" class="get-answer-link" data-toggle="modal" data-target="#getAnswersCommunityModal">Get Answers</a>
		                        		<?php if($_POST['gasubmit']): ?>
				                            <script>
				                                jQuery(document).ready(function() {
				                                    jQuery('a.get-answer-link').trigger('click');
				                                });
				                            </script>
		                        		<?php endif; ?>
		                        	</div>
		                            <div class="clearthis"></div>
		                        </div>
	                    
	                    
	                        <?php if(isset($_GET['comunityid']) || ($community_id != '#')): ?>
		                        <div class="item-box">
		                        	<a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon4.png" class="sidebar-r-ico" /></a>
		                        	<div class="sidebar-r-text">
		                        		<a href="<?php bloginfo('url'); ?>/community-blog/?id=<?php echo $community_id; ?>">Community Blog</a>
		                        	</div>
		                        	<div class="clearthis"></div>
		                        </div>
	                        <?php endif; ?>
	                    
	                    
	                    
	                    <?php if ( is_user_logged_in() ) { ?>

		                    <!-- scorecard -->

		                    <div class="scorecard">
		                        <div class="scorecard-content">
		                            <?php wpfp_property_link(0, "", 0, array( 'listing_key' => ((string)$mylisting->ListingKey) )) ?>
		                        </div>
		                    </div>
	                    
	                    <?php } else { echo "<br/>";} ?>
	                    
	                    
	                    <!-- more links -->
	                    
	                    <div class="more-links">
	                        <a href="#" onclick="window.print();return false" ><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-print.png" alt="Print" class="ml-iconf" /></a>
	                        <a href="mailto:<?php //the_field('communityemail')?>"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-mail.png" alt="Mail" class="ml-icon" /></a>
	                        <a href="#" data-toggle="modal" data-target="#shareModal"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-share.png" alt="Share" class="ml-icon" /></a>
	                    </div>

	                    <!-- Share THis  -->
	                    <?php  share_this_items( $_SERVER['HTTP_HOST']."/property-searchs?listingKey=".$mylisting->ListingKey, "Property" ); ?>

	                    <?php if($agent): ?>
	                    	<div class="e-agentbt"><a href="mailto:<?php echo get_the_author_meta('user_email', $agent['ID']); ?>">Email Agent</a></div>
	                    <?php endif; ?>
	            
	                </div>



	                <?php endif; ?>
	        <?php endforeach; ?>
	        <?php else: echo "No properties found on your scorecard."; ?>
	        <?php endif; ?>


	    <?php
	}




	public function comdest_favorite_right_content_with_id($type, $favorite_post_ids) {
		$id = mysql_real_escape_string(trim($_GET['id']));

		$args = array(
    		'p' => $id,
    		'post_type' => $type,
    		'post_status' => 'publish'
    	);

    	$the_query = new WP_Query( $args );

    	echo '<div id="sidebar-r" class="row">';

    	if($the_query->have_posts()) :
    		while($the_query->have_posts()) :
    			$the_query->the_post();

    			if($type == 'communities'):
    				$this->community_right_section();
    			elseif($type == 'destinations'):
    				$this->destination_right_section();
    			endif;
    		endwhile;
    	else:
    		echo "";
    	endif;

    	wp_reset_query();

    	echo '</div>';
	}




	public function comdest_favorite_right_content($type, $favorite_post_ids) {
		echo '<div id="sidebar-r" class="row">';
		$ctr = 0;
        if ($favorite_post_ids) { 
            //var_dump($favorite_post_ids);
            $favorite_post_ids = array_reverse($favorite_post_ids);

            $qry = array('post__in' => $favorite_post_ids, 'orderby' => 'post__in');
            // custom post type support can easily be added with a line of code like below.
            $qry['post_type'] = array($type);
            $qry['posts_per_page'] = 1; 
            $post_in = count($qry['post__in']);
            query_posts($qry);

        if(($post_in != 0)) {
            while ( have_posts() ) : $ctr++; the_post(); $picture = wp_get_attachment_image_src( get_post_thumbnail_id(), 'scorecard-com-thumb' );
				 $the_location = get_post_meta(get_the_ID(), 'location_2');

                 if($type == 'communities'):
    				$this->community_right_section();
    			elseif($type == 'destinations'):
    				$this->destination_right_section();
    			endif;

			endwhile;  
                
        }  

        if($ctr == 0) :
			if($type == 'communities'):
            	echo "<p class='no_item_message center'>No communities found.</p>";
           	elseif($posttype == 'destinations'):
           		echo "<p class='no_item_message center'>No destinations found.</p>";
           	endif;
        endif;

        	wp_reset_postdata();
        	wp_reset_query();

        } else {
            if($type == 'communities'):
           		echo "<p class='no_item_message center'>No communities found.</p>";
            elseif($type == 'destinations'):
           		echo "<p class='no_item_message center'>No destinations found.</p>";
            endif;
        }
        echo "<span id='desti_count_hidden' style='display: none;'>".$ctr."</span>";


        echo '</div>';

	} 



	public function destination_right_section() {
		?>
			<div class="item-box">
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon1.png" class="sidebar-r-ico" /></a>
                <div class="sidebar-r-text">
                	<a href="<?php bloginfo('url'); ?>/property-search/?destinationid=<?php echo get_the_ID(); ?>&destproperty-search=true">
                		Search All Properties in this Golf Destination
                	</a>
                </div>
                <div class="clearthis"></div>
            </div>


            <div class="item-box">
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon2.png" class="sidebar-r-ico" /></a>
                <div class="sidebar-r-text">
                	<a href="#" data-toggle="modal" data-target="#viewDestinationAgentModal">View Agents Specialized in this Golf Destination</a>
                </div>
                <div class="clearthis"></div>
                <?php 
                	$destination_class = new Destinations;
                	$destination_class->destination_view_agents_modal();
               	?>
            </div>

			
			<div class="item-box">
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon3.png" class="sidebar-r-ico" /></a>
                <div class="sidebar-r-text">
                	<a href="#" class="get-answer-link" data-toggle="modal" data-target="#getAnswersCommunityModal">Get Answers</a>
	                <?php if($_POST['gasubmit']): ?>
	                    <script>
	                        jQuery(document).ready(function() {
	                            jQuery('a.get-answer-link').trigger('click');
	                        });
	                    </script>
	                <?php endif; ?>
                </div>
                <div class="clearthis"></div>
            </div>


            <?php if ( is_user_logged_in() ) : ?>
	            <div class="scorecard">
	                <div class="scorecard-content">
	                    <?php wpfp_link() ?>
	                </div>
	            </div>
            <?php else: ?> 
            	<br/>
            <?php endif; ?>

            <!-- more links -->
             <div class="more-links">
                <a href="#" onclick="window.print();return false"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-print.png" alt="Print" class="ml-iconf" /></a>
                <a href="mailto:<?php the_field('destinationemail')?>"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-mail.png" alt="Mail" class="ml-icon" /></a>
                <a href="#" data-toggle="modal" data-target="#shareModal"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-share.png" alt="Share" class="ml-icon" /></a>
            </div>

            <!-- Share THis  -->
            <?php share_this_items( get_the_permalink(), get_the_title() ); ?>

        	<div id="experts-box">
	            <div class="title-box">
	            	<h2><?php the_title(); ?> Experts</h2>
	            </div>

	            <?php 
	            	$email_array = array(); 
	            	$agents = get_field('destinationagents');
	            ?>
				<?php if($agents) : ?>
					<?php foreach($agents as $agent) : ?>
						<?php
							$agent_id = $agent['ID'];
	                        $agent_name = $agent['display_name'];
	                        $agent_phone = get_field('agentcontact','user_'.$agent_id);
	                        $agent_pic = get_field('profile_picture','user_'.$agent_id);
	                        $agent_position = get_field('agentposition','user_'.$agent_id);
	                        $agent_email = get_the_author_meta('user_email',$agent_id); 

	                        //push email so email array
	                        array_push($email_array, $agent_email);
						?>

						<div class="item-expert">
							<?php if($agent_pic) : ?>
								<img src="<?php echo $agent_pic['sizes']['thumbnail']; ?> " class="expert-img" style="width: 74px; height: 74p;" />
							<?php else: ?>
								<img src="<?php bloginfo('template_directory');?>/img/default_avatar.jpg" class="expert-img" style="width: 74px; height: 74px;" />
							<?php endif; ?>
							<div class="expert-desc">
		                        <h3><?php echo $agent_name; ?></h3>
		                        <?php echo $agent_position; ?>
		                        <?php echo $agent_phone; ?>
		                        <?php //the_field('agentdesignations','user_'.$agent_id);?>
		                        <br />
		                        <a href="<?php echo site_url(); ?>/agents/?id=<?php echo $agent_id; ?>">View more details »</a>
		                    </div>
		                    
		                    <div class="clearthis"></div>
		                
		                </div>
		            <?php endforeach; ?>
				<?php else: ?> 
					<p class="center">No experts found.</p>
				<?php endif; ?>

				<!-- Get answers Modal -->
            	<?php get_answers_community_modal($email_array); ?>
   			</div>

		<?php
	}


	public function community_right_section() {
		?>
			
			<div class="item-box">
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon1.png" class="sidebar-r-ico" /></a>
                <div class="sidebar-r-text">
                	<a href="<?php bloginfo('url'); ?>/property-search/?comunityid=<?php echo get_the_ID(); ?>&comproperty-search=true">
                		Search All Properties in this Golf Community
                	</a>
                </div>
                <div class="clearthis"></div>
            </div>


            <div class="item-box">
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon2.png" class="sidebar-r-ico" /></a>
                <div class="sidebar-r-text">
                	<a href="#" data-toggle="modal" data-target="#viewCommunityAgentModal">View Agents Specialized in this Golf Community</a>
                </div>
                <div class="clearthis"></div>
                <?php
                	$community_class = new Communities; 
                	$community_class->community_view_agents_modal(); 
                ?>
            </div>

			
			<div class="item-box">
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon3.png" class="sidebar-r-ico" /></a>
                <div class="sidebar-r-text">
                	<a href="#" class="get-answer-link" data-toggle="modal" data-target="#getAnswersCommunityModal">Get Answers</a>
	                <?php if($_POST['gasubmit']): ?>
	                    <script>
	                        jQuery(document).ready(function() {
	                            jQuery('a.get-answer-link').trigger('click');
	                        });
	                    </script>
	                <?php endif; ?>
                </div>
                <div class="clearthis"></div>
            </div>

            <div class="item-box">
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon4.png" class="sidebar-r-ico" /></a>
                <div class="sidebar-r-text">
                	<a href="<?php bloginfo('url'); ?>/community-blog/?id=<?php echo get_the_ID(); ?>">Community Blog</a>
                </div>
                <div class="clearthis"></div>
            </div>


            <?php if ( is_user_logged_in() ): ?>
	            <div class="scorecard">
	                <div class="scorecard-content">
	                    <?php wpfp_link() ?>
	                </div>
	            </div>
            <?php else: ?>
            	<br/>
            <?php endif; ?>

            <!-- more links -->
             <div class="more-links">
                <a href="#" onclick="window.print();return false"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-print.png" alt="Print" class="ml-iconf" /></a>
                <a href="mailto:<?php the_field('communityemail')?>"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-mail.png" alt="Mail" class="ml-icon" /></a>
                <a href="#" data-toggle="modal" data-target="#shareModal"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-share.png" alt="Share" class="ml-icon" /></a>
            </div>

            <!-- Share THis  -->
            <?php share_this_items( get_the_permalink(), get_the_title() ); ?>

        	<div id="experts-box">
	            <div class="title-box">
	            	<h2><?php the_title(); ?> Experts</h2>
	            </div>

	            <?php 
	            	$email_array = array(); 
	            	$agents = get_field('communityagents');
	            ?>
				<?php if($agents): ?>
					<?php foreach($agents as $agent): ?>
						<?php
							$agent_id = $agent['ID'];
	                        $agent_name = $agent['display_name'];
	                        $agent_phone = get_field('agentcontact','user_'.$agent_id);
	                        $agent_pic = get_field('profile_picture','user_'.$agent_id);
	                        $agent_position = get_field('agentposition','user_'.$agent_id);
	                        $agent_email = get_the_author_meta('user_email',$agent_id); 

	                        //push email so email array
	                        array_push($email_array, $agent_email);
						?>

						<div class="item-expert">
							<?php if($agent_pic): ?>
								<img src="<?php echo $agent_pic['sizes']['thumbnail']; ?> " class="expert-img" style="width: 74px; height: 74p;" />
							<?php else: ?>
								<img src="<?php bloginfo('template_directory');?>/img/default_avatar.jpg" class="expert-img" style="width: 74px; height: 74px;" />
							<?php endif; ?>
							<div class="expert-desc">
		                        <h3><?php echo $agent_name; ?></h3>
		                        <?php echo $agent_position; ?>
		                        <?php echo $agent_phone; ?>
		                        <?php //the_field('agentdesignations','user_'.$agent_id);?>
		                        <br />
		                        <a href="<?php echo site_url(); ?>/agents/?id=<?php echo $agent_id; ?>">View more details »</a>
		                    </div>
		                    
		                    <div class="clearthis"></div>
		                
		                </div>
					<?php endforeach; ?>
				<?php else: ?>
					<p class="center">No experts found.</p>
				<?php endif; ?>

				<!-- Get answers Modal -->
            	<?php get_answers_community_modal($email_array); ?>
	        </div>
		

		<?php
	}






}


$scorecard_class = new Scorecard;