<?php
 /*
  * Dropdown Location Select Functions
  */

function rem_get_countries()
{
    global $wpdb;
    $countries_db = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."countries ORDER BY country ASC");

    $countries = array();

    foreach ($countries_db AS $country)
    {
        if (trim($country->country) == '') continue;
        $countries[$country->id] = $country->country;
    }

    return $countries;
}


add_action('wp_ajax_rem_get_cities','rem_get_cities');
add_action('wp_ajax_nopriv_rem_get_cities','rem_get_cities');
function rem_get_cities() {
    $country_id = mysql_real_escape_string(trim($_POST['vu']));
    $if_us = mysql_real_escape_string(trim($_POST['if_us']));
    //get cities with country id

    if($if_us != 'yes') {
        echo rem_get_country_cities($country_id);
    } else {
        echo rem_get_us_state($country_id);
    }
    

    die();
}

function rem_get_country_cities($country_id) {
    global $wpdb;

    $cities_db        = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."cities WHERE  country='".$country_id."' ORDER BY city ASC");
    $cities               = array();

    if ($cities_db)
    {
        foreach ($cities_db AS $city)
        {
            $cities[$city->id] = $city->city;
        }
    }

    return json_encode($cities);

    die();
}

function rem_get_us_state($country_id) {
    global $wpdb;

    $states_db        = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."states ORDER BY state ASC");
    $states               = array();

    if ($states_db)
    {
        foreach ($states_db AS $state)
        {
            $states[$state->id] = $state->state;
        }
    }

    echo json_encode($states);

    die();
}


add_action('wp_ajax_rem_get_country_cities_with_state','rem_get_country_cities_with_state');
add_action('wp_ajax_nopriv_rem_get_country_cities_with_state','rem_get_country_cities_with_state');
function rem_get_country_cities_with_state() {
    global $wpdb;
    $country_id =  mysql_real_escape_string(trim($_POST['country_id']));
    $state_id =  mysql_real_escape_string(trim($_POST['state_id']));

    $cities_db        = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."cities WHERE country='".$country_id."' AND state='".$state_id."' ORDER BY city ASC");
    $cities               = array();

    if ($cities_db)
    {
        foreach ($cities_db AS $city)
        {
            $cities[$city->id] = $city->city;
        }
    }

    echo json_encode($cities);

    die();
}

if(!function_exists('hm_get_country_code'))
{
    function hm_get_country_code($country_id) {
        global $wpdb;
        $country_id = mysql_real_escape_string(trim($country_id));

        $country_db  = $wpdb->get_results("SELECT code FROM ".$wpdb->prefix."countries WHERE id='{$country_id}'");
        $states     = array();

        if ($country_db)
        {
            return $country_db[0]->code;
        }

        return FALSE;
    }
}

if(!function_exists('hm_get_state_code'))
{
    function hm_get_state_code($state_id) {
        global $wpdb;
        $state_id = mysql_real_escape_string(trim($state_id));

        $state_db  = $wpdb->get_results("SELECT code FROM ".$wpdb->prefix."states WHERE id='{$state_id}'");
        $states     = array();

        if ($state_db)
        {
            return $state_db[0]->code;
        }

        return FALSE;
    }
}

if(!function_exists('hm_get_city'))
{
    function hm_get_city($city_id) {
        global $wpdb;
        $city_id = mysql_real_escape_string(trim($city_id));

        $city_db  = $wpdb->get_results("SELECT city FROM ".$wpdb->prefix."cities WHERE id='{$city_id}'");
        $states     = array();

        if ($city_db)
        {
            return $city_db[0]->city;
        }

        return FALSE;
    }
}
