<?php

/*
 * MEtaboxes
 */

function comdest_property_meta_box() {

    $screens = array( 'communities', 'destinations' );

    foreach ( $screens as $screen ) {

        add_meta_box(
            'comdest_property_sectionid','Properties',
            'comdest_property_callback',
            $screen
        );
    }
}
add_action( 'add_meta_boxes', 'comdest_property_meta_box' );




function comdest_property_callback( $post ) {

    // Add an nonce field so we can check for it later.
    wp_nonce_field( 'comdest_property_meta_box', 'comdest_property_meta_box_nonce' );

    /*
     * Use get_post_meta() to retrieve an existing value
     * from the database and use the value for the form.
     */
    $property_value = get_post_meta( $post->ID, '_comdest_property_value_key', true );


    $property_value = $property_value ? unserialize($property_value):array();

    echo '<label for="sample_field">';
    _e( 'Add properties to this community or destination');
    echo '</label> ';

    //echo '<input type="text" id="sample_field" name="sample_field" value="' . esc_attr( $value ) . '" size="25" />';
    

    if(function_exists('property_search')) {

        list($listing, $price) = property_search('admin');
    ?>
        <table id="propertyTable" class="display">
            <thead>
                <tr>
                    <th>Add</th>
                    <th>Listing Key</th>
                    <th>Property Title</th>
                    <th>Property Price</th>
                </tr>
            </thead>
            <tbody>

                <?php foreach($listing as $key => $value) { ?>
                    <tr>
                        <td><input type="checkbox" name="property_field[]" value="<?php echo $value->ListingKey; ?>" <?php echo in_array((string)$value->ListingKey,$property_value) ? 'checked':'' ?> /> </td>
                        <td><?php echo $value->ListingKey; ?></td>
                        <td><?php echo $value->ListingTitle; ?></td>
                        <td><?php echo '$'.number_format((string)$value->ListPrice,2); ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <script>
        jQuery(document).ready(function($){
            $('#propertyTable').dataTable({
                "sScrollY": "450px",
                "bPaginate": false,

            });
        });
        </script>
    <?php

    }
}


function comdest_property_save_meta_box_data( $post_id ) {

    /*
     * We need to verify this came from our screen and with proper authorization,
     * because the save_post action can be triggered at other times.
     */

    // Check if our nonce is set.
    if ( ! isset( $_POST['comdest_property_meta_box_nonce'] ) ) {
        return;
    }

    // Verify that the nonce is valid.
    if ( ! wp_verify_nonce( $_POST['comdest_property_meta_box_nonce'], 'comdest_property_meta_box' ) ) {
        return;
    }

    // If this is an autosave, our form has not been submitted, so we don't want to do anything.
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return;
    }

    // Check the user's permissions.
    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {

        if ( ! current_user_can( 'edit_page', $post_id ) ) {
            return;
        }

    } else {

        if ( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }
    }

    /* OK, it's safe for us to save the data now. */
    
    // Make sure that it is set.
    if ( ! isset( $_POST['property_field'] ) ) {
        return;
    }

    // Sanitize user input.
    $my_data = $_POST['property_field'];

    $properties = serialize($my_data);

    // Update the meta field in the database.
    update_post_meta( $post_id, '_comdest_property_value_key', $properties );
}
add_action( 'save_post', 'comdest_property_save_meta_box_data' );