@layout('layouts.master')


@section('left-section')
	<div class="sr-headline row">
		@if(isset($_GET['gcountry']) && isset($_GET['gstate']) && isset($_GET['gcity']))
			<h2>Search Results</h2>
		@else
			<h2>List of Communities</h2>
		@endif
		<div class="sub-head"></div>
	</div>
	<div id="search-results-box">
		@if(isset($_GET['gcountry']) && isset($_GET['gstate']) && isset($_GET['gcity']) && is_user_logged_in())
	    	<input type="text" class="save-search-title" style="width: 99%;margin-top: 5px;" placeholder="Name your search..." link="<?php echo $_SERVER['REQUEST_URI']; ?>" />
	    	<a href="#" class="sr-edit" data-target="#myModald" data-toggle="modal">edit search</a> <a href="#" class="sr-save">save search</a>
	    @endif
	    <div id="search-content">

    		<?php $args = $community_class->get_community_arg(-1); $ctr = 0; ?>
			
			@wpquery($args)
				 <?php $ctr++; ?>
				 <div class="search-item">
					@if( has_post_thumbnail() )
						<div class="comdest-search-thumb">
							{{ the_post_thumbnail('scorecard-com-thumb') }}
						</div>
					@else
						<div class="comdest-search-thumb">
                        	<img src="<?php bloginfo('template_directory');?>/img/img_not_available.jpg" class="img-responsive" />
                        </div>
					@endif

					@if(($country_id != "") || ($state_id != "") || ($city_id != ""))
			        	<h4><a href="<?php the_permalink(); ?>?gcountry=<?php echo $country_id; ?>&gstate=<?php echo $state_id; ?>&gcity=<?php echo $city_id; ?>">{{ the_title() }}</a></h4>
			        @else
			        	<h4><a href="<?php the_permalink(); ?>">{{ the_title() }}</a></h4>
			        @endif



			        <div class="sr-desc">
						<?php 
							$desc = get_field('communitydescription');
							$position = stripos ($desc, ".");

							if($position) { //if there's a dot in our soruce text do
			                    $offset = $position + 1; //prepare offset
			                    $position2 = stripos ($desc, ".", $offset); //find second dot using offset
			                    $first_two = substr($desc, 0, $position2); //put two first sentences under $first_two

			                    echo $first_two . '.'; //add a dot
			                }
						?>
			        </div>
				 </div>
			@wpempty
				<p>No Communities found.</p> 
			@wpend

			<span style="display: none;" class="search-count2">{{ $ctr }} matches</span>
            <script>
                jQuery(document).ready(function($) {
                    var count = $('.search-count2').html();
                    $('.sr-headline .sub-head').html(count);
                });
            </script>

	    </div>
	</div>
@endsection


@section('tab-links')
	<div id="page-links">
	    @include('includes/tab/tab-links' )
	</div>
@endsection


@section('main-content')

		@wpposts
			<div id="mid-col-main">
		        <div class="featured-image">
		            <div class="tab-content">
		                <div class="tab-pane active" id="community">
		                     @include('includes/tab/community-details-tab-content')
		                </div> 
		                <!-- end community tab -->

		                <div class="tab-pane" id="photos">
		                    @include('includes/tab/community-photos-tab-content' )
		                </div>
		                <!-- end photos tab -->

		                <div class="tab-pane" id="location">
		                    @include('includes/tab/community-location-tab-content' )
		                </div>
		                <!--end location tab -->

		                <div class="tab-pane" id="video">
		                    @include('includes/tab/community-video-tab-content' )
		                </div>
		                <!-- end vides tab -->
		            </div> <!-- end tab-content -->                        
		        </div><!-- END featured image -->
		    </div><!-- END MAIN -->
	    @wpempty

	    @wpend
@endsection



@section('right-section')
	<div id="sidebar-r" class="row">

		@wpposts
			<div class="item-box">
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon1.png" class="sidebar-r-ico" /></a>
                <div class="sidebar-r-text">
                	<a href="<?php bloginfo('url'); ?>/property-search/?comunityid=<?php echo get_the_ID(); ?>&comproperty-search=true">
                		Search All Properties in this Golf Community
                	</a>
                </div>
                <div class="clearthis"></div>
            </div>


            <div class="item-box">
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon2.png" class="sidebar-r-ico" /></a>
                <div class="sidebar-r-text">
                	<a href="#" data-toggle="modal" data-target="#viewCommunityAgentModal">View Agents Specialized in this Golf Community</a>
                </div>
                <div class="clearthis"></div>
                <?php $community_class->community_view_agents_modal(); ?>
            </div>

			
			<div class="item-box">
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon3.png" class="sidebar-r-ico" /></a>
                <div class="sidebar-r-text">
                	<a href="#" class="get-answer-link" data-toggle="modal" data-target="#getAnswersCommunityModal">Get Answers</a>
	                <?php if($_POST['gasubmit']): ?>
	                    <script>
	                        jQuery(document).ready(function() {
	                            jQuery('a.get-answer-link').trigger('click');
	                        });
	                    </script>
	                <?php endif; ?>
                </div>
                <div class="clearthis"></div>
            </div>

            <div class="item-box">
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon4.png" class="sidebar-r-ico" /></a>
                <div class="sidebar-r-text">
                	<a href="<?php bloginfo('url'); ?>/community-blog/?id=<?php echo get_the_ID(); ?>">Community Blog</a>
                </div>
                <div class="clearthis"></div>
            </div>


            @if ( is_user_logged_in() )
	            <div class="scorecard">
	                <div class="scorecard-content">
	                    <?php wpfp_link() ?>
	                </div>
	            </div>
            @else 
            	<br/>
            @endif

            <!-- more links -->
             <div class="more-links">
                <a href="#" onclick="window.print();return false"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-print.png" alt="Print" class="ml-iconf" /></a>
                <a href="mailto:<?php the_field('communityemail')?>"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-mail.png" alt="Mail" class="ml-icon" /></a>
                <a href="#" data-toggle="modal" data-target="#shareModal"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-share.png" alt="Share" class="ml-icon" /></a>
            </div>

            <!-- Share THis  -->
            <?php share_this_items( get_the_permalink(), get_the_title() ); ?>

        	<div id="experts-box">
	            <div class="title-box">
	            	<h2>{{ the_title() }} Experts</h2>
	            </div>

	            <?php 
	            	$email_array = array(); 
	            	$agents = get_field('communityagents');
	            ?>
				@if($agents)
					@forelse($agents as $agent)
						<?php
							$agent_id = $agent['ID'];
	                        $agent_name = $agent['display_name'];
	                        $agent_phone = get_field('agentcontact','user_'.$agent_id);
	                        $agent_pic = get_field('profile_picture','user_'.$agent_id);
	                        $agent_position = get_field('agentposition','user_'.$agent_id);
	                        $agent_email = get_the_author_meta('user_email',$agent_id); 

	                        //push email so email array
	                        array_push($email_array, $agent_email);
						?>

						<div class="item-expert">
							@if($agent_pic)
								<img src="{{ $agent_pic['sizes']['thumbnail'] }} " class="expert-img" style="width: 74px; height: 74p;" />
							@else
								<img src="<?php bloginfo('template_directory');?>/img/default_avatar.jpg" class="expert-img" style="width: 74px; height: 74px;" />
							@endif
							<div class="expert-desc">
		                        <h3>{{ $agent_name }}</h3>
		                        {{ $agent_position }}
		                        {{ $agent_phone }}
		                        <?php //the_field('agentdesignations','user_'.$agent_id);?>
		                        <br />
		                        <a href="{{ site_url() }}/agents/?id={{ $agent_id }}">View more details »</a>
		                    </div>
		                    
		                    <div class="clearthis"></div>
		                
		                </div>
					@empty
						
					@endforelse
				@else
					<p class="center">No experts found.</p>
				@endif

				<!-- Get answers Modal -->
            	<?php get_answers_community_modal($email_array); ?>
	        </div>

		@wpempty
		@wpend

	</div>
@endsection





