<?php

//aamrole_53ec21a063e46 - Agent Role


require_once('functions/posttype-functions.php');
require_once('functions/community-functions.php');
require_once('functions/destination-functions.php');
require_once('functions/agent-functions.php');
require_once('functions/frontnine-functions.php');
require_once('functions/locationselect-functions.php');
require_once('functions/widget-functions.php');
require_once('functions/other-functions.php');
require_once('functions/scorecard-functions.php');
require_once('functions/metaboxes.php');

add_action('init','hook_css');
function hook_css(){
     wp_localize_script( 'ajax-script', 'remielPluginAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
}


//thumbnails
add_theme_support( 'post-thumbnails' );

if ( function_exists( 'add_theme_support' ) ) {
	add_image_size( 'page-thumb', 459, 303, false );
	add_image_size( 'scorecard-com-thumb', 180, 115, true );
	add_image_size( 'scorecard-agent-thumb', 110, 124, true );
	add_image_size( 'search-results-thumbnail',100, 75, true );
	add_image_size( 'home-photo-details',225, 169, true );
	add_image_size( 'large-photo-photos',465, 350, true );
	add_image_size( 'thumbnail-photo-photos',91, 68, true );
}

//add 'home' automatically to menu
function my_page_menu_args($args) {
	$args['show_home'] = true;
	return $args;
}
add_filter('wp_page_menu_args', 'my_page_menu_args');


//register menus
function register_my_menus() {
	register_nav_menus(
		array(
			'main-menu' => __( 'Top Navigation' ),
			'footer-menu' => __( 'Footer Navigation' ),
		)
	);
}
add_action( 'init', 'register_my_menus' );


//excerpt
function get_excerpt_by_id($post_id){
	$the_post = get_post($post_id); //Gets post ID
	$the_excerpt = $the_post->post_content; //Gets post_content to be used as a basis for the excerpt
	$excerpt_length = 100; //Sets excerpt length by word count
	$the_excerpt = strip_tags(strip_shortcodes($the_excerpt)); //Strips tags and images
	$words = explode(' ', $the_excerpt, $excerpt_length + 1);
	if(count($words) > $excerpt_length) :
		array_pop($words);
		array_push($words, ' ');
		$the_excerpt = implode(' ', $words);
	endif;
		$the_excerpt = '<p>' . $the_excerpt . '</p>';
	return $the_excerpt;
}
add_filter('excerpt_more','__return_false');


//Random Password Generator
function rand_passwd( $length = 8, $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789' ) {
    return substr( str_shuffle( $chars ), 0, $length );
}


//Ultimate wp query search filter
add_filter('uwpqsf_result_tempt', 'customize_output', '', 4);
function customize_output($results , $arg, $id, $getdata ){
     // The Query
            $apiclass = new uwpqsfprocess();

            $last_meta_query = $arg['meta_query'];
            unset($arg['meta_query']);

            $query = new WP_Query( $arg );

            unset($arg['s']);
            $arg['meta_query'] = $last_meta_query;
            //unset($arg['meta_query'][1]);

            $query2 = new WP_Query( $arg );



            $merged = array_merge( $query->posts, $query2->posts );

			$post_ids = array();
			foreach( $merged as $item ) {
			    $post_ids[] = $item->ID;
			}

			$unique = array_unique($post_ids);


            $arg['post__in'] = $unique;

            unset($arg['meta_query']);
            unset($arg['tax_query']);

            $query3 = new WP_Query( $arg );


			 //$query = $query3;



        ob_start();    $result = '';
            // The Loop
 		$ctr = 0;
        if ( $query3->have_posts() ) { 
            while ( $query3->have_posts() ) {
            	
                $query3->the_post();


                if(count($arg['post__in'])==0) {
                    ?>
                        <span style="display: none;" class="search-count"><?php echo $ctr; ?> matches</span>
                        <script>
                        jQuery(document).ready(function($) {
                            var count = $('.search-count').html();
                            $('.sr-headline .sub-head').html(count);
                        });
                        </script>
                        <p>No 
                            <?php 

                                if($arg['post_type'][0] == 'communities'):
                                    echo "Community ";
                                elseif($arg['post_type'][0] == 'destinations'):
                                    echo "Destination ";
                                elseif($arg['role'] == 'agents'):
                                    echo "Agents ";
                                endif;
                            ?>

                        was found.</p>
                    <?php die();
                }

                $ctr++;

     ?>

 					<!-- search item -->
                    <div class="search-item">
                    	 
                        <?php
                        	//print_r($arg);
                            if ( has_post_thumbnail() ) {
                                the_post_thumbnail('small');  
                            }else{
                        ?>
                         <img src="<?php bloginfo('template_directory');?>/img/sample-community-pic.jpg" class="img-responsive" />
                         <?php
                            }
                        ?>
                        <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                        <div class="sr-desc">
                       		 <?php 
                       		 	if(get_field('communitydescription')) {
                                	$desc=get_field('communitydescription');
                                } elseif (get_field('destinationdescription')) {
                                	$desc=get_field('destinationdescription');
                                }
                                $position = stripos ($desc, "."); //find first dot position

                                if($position) { //if there's a dot in our soruce text do
                                    $offset = $position + 1; //prepare offset
                                    $position2 = stripos ($desc, ".", $offset); //find second dot using offset
                                    $first_two = substr($desc, 0, $position2); //put two first sentences under $first_two

                                    echo $first_two . '.'; //add a dot
                                }

                                else {  //if there are no dots
                                    //do nothing
                                }
                            ?> 
                        </div>
                    
                    </div>
                    <!-- end search item -->

     <?php
            }
     ?>
     	<span style="display: none;" class="search-count"><?php echo $ctr; ?> matches</span>
		<script>
		jQuery(document).ready(function($) {
			var count = $('.search-count').html();
			$('.sr-headline .sub-head').html(count);
		});
		</script>
     <?php
                        echo  $apiclass->ajax_pagination($arg['paged'],$query->max_num_pages, 4, $id, $getdata);
         } else {
     ?>
		<span style="display: none;" class="search-count"><?php echo $ctr; ?> matches</span>
		<script>
		jQuery(document).ready(function($) {
			var count = $('.search-count').html();
			$('.sr-headline .sub-head').html(count);
		});
		</script>
        <p>No 
            <?php 
                if($arg['post_type'][0] == 'communities'):
                    echo "Community ";
                elseif($arg['post_type'][0] == 'destinations'):
                    echo "Destination ";
                elseif($arg['role'] == 'agents'):
                    echo "Agents ";
                endif;
            ?>

            was found.</p>
    <?php
                }
                /* Restore original Post Data */
                wp_reset_query();
 
        $results = ob_get_clean();        
            return $results;
}


//share this items
function share_this_items( $permalink, $title ) {
    ?>
    <!-- SHARE -->
    <div id="shareModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content boxtype">
                <div class="modal-header noborders">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title mypopuptitle" id="myModalLabel">Social Share</h4>
                </div>
                <div class="modal-body">
                    <div class="addthis_toolbox addthis_default_style addthis_32x32_style" addthis:url="<?php echo $permalink; ?>" addthis:title="Finest Golf Property - <?php echo $title; ?>">
                        <a class="addthis_button_facebook at300b" title="Facebook" href="#"><span class=" at300bs at15nc at15t_facebook"><span class="at_a11y">Share on facebook</span></span></a>
                        <a class="addthis_button_twitter at300b" title="Tweet" href="#"><span class=" at300bs at15nc at15t_twitter"><span class="at_a11y">Share on twitter</span></span></a>
                        <a class="addthis_button_email at300b" target="_blank" title="Email" href="#"><span class=" at300bs at15nc at15t_email"><span class="at_a11y">Share on email</span></span></a>
                        <a class="addthis_button_pinterest_share at300b" target="_blank" title="Pinterest" href="#"><span class=" at300bs at15nc at15t_pinterest_share"><span class="at_a11y">Share on pinterest_share</span></span></a>
                        <a class="addthis_button_compact at300m" href="#"><span class=" at300bs at15nc at15t_compact"><span class="at_a11y">More Sharing Services</span></span></a>
                        <a class="addthis_counter addthis_bubble_style" href="#" tabindex="1000" style="display: inline-block;"><a class="addthis_button_expanded" target="_blank" title="View more services" href="#">5</a>
                        <a class="atc_s addthis_button_compact"><span></span></a></a>
                        <div class="atclear"></div>
                    </div>
                </div>
            </div>
        </div>  
    </div>

    <?php
}








add_action("wp_ajax_hm_scorecard_email_alert", "hm_scorecard_email_alert");
add_action("wp_ajax_nopriv_hm_scorecard_email_alert", "hm_scorecard_email_alert");
function hm_scorecard_email_alert() {
    $test = get_option('test-my-time-to-alert', '1970-01-01 00:00:00');
    $users = get_users("orderby=registered&order=ASC");
    foreach ($users as $user) {
        $fave = array();
        $usermeta_wpfp_favorites = get_user_meta($user->ID, 'wpfp_favorites', true);
        $usermeta_wpfp_favorites_agents = get_user_meta($user->ID, 'wpfp_favorites_agents', true);
        $usermeta_wpfp_favorites_properties = get_user_meta($user->ID, 'wpfp_favorites_properties', true);

        //destination & community
        if (!empty($usermeta_wpfp_favorites)) {
            foreach ($usermeta_wpfp_favorites as $favorite) {
                $favorite_post = get_post($favorite);
                //check if the "test-my-time-to-alert" is less than or equal to "post_modified"
                if (strtotime($test) <= strtotime($favorite_post->post_modified)) {
                    //alert the user that this post is updated :)
                    $fave[$favorite_post->post_type][] = '<a href="'.get_permalink($favorite).'">'.$favorite_post->post_title.'</a>';
                }
            }
        }
        //agent
        if (!empty($usermeta_wpfp_favorites_agents)) {
            foreach ($usermeta_wpfp_favorites_agents as $favorite) {
                $favorite_user = get_user($favorite);
                $fave['agent'][] = '<a href="'.get_bloginfo('url').'/agents/?id='.$favorite.'">'.$favorite_user->display_name.'</a>';
            }
        }
        //properties
        if (!empty($usermeta_wpfp_favorites_properties)) {
            list($properties, $price) = property_search('admin');
            foreach ($usermeta_wpfp_favorites_properties as $favorite) {
                $favorite_property = $properties[$favorite];
                //echo $favorite_property->ModificationTimestamp;
                if (strtotime($test) <= strtotime((string)$favorite_property->ModificationTimestamp)) {
                    $fave['property'][] = '<a href="'.get_bloginfo('url').'/property-search/?listingKey='.$favorite.'">'.$favorite_property->ListingTitle.'</a>';
                }
            }
        }
        //print_r($user);
        if (count($fave)) {
            $subj = 'Sample Email Alert';
            $msg = "Hello {$user->data->display_name},<br/><br/>";
            $msg = "Here are the updates in your scoreboard:<br/>";
            foreach($fave as $type => $arr){
                $msg .= "<strong>".ucwords($type)."</strong>:<br/>";
                $msg .= implode("<br/>", $arr) . "<br/>";
            }
            $headers = 'From: Finest Golf Properties<info@finestgolfproperty.com>' . "\r\n";
            $headers .= 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            //wp_mail( 'remiel@august99.com', $subj, $msg, $headers );
            //wp_mail( 'james.songalia@august99.com', $subj, $msg, $headers );
            wp_mail( $user->data->user_email, $subj, $msg, $headers );
        }
    }
    //update the "test-my-time-to-alert" to now
    update_option('test-my-time-to-alert', date('Y-m-d h:i:s'));
    die();
}







