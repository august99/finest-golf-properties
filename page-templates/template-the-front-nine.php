@layout('layouts.wide')

<?php 
/*
 * Template Name: The Front Nine Page Template 
 */
?>

@section('main-content')


<div class="sr-headline">
    <h2>The Front Nine</h2>
</div>

 <ul>
	<?php $properties = property_search('admin');
		$firstkey = '';
		$frontnine = hayme_get_front_nine();
		foreach($frontnine as $key) { $frontninedata = $properties[0][$key]; 

			$listing_address = $frontninedata->Address->children('commons', true);
	?>
	    	<li>
	    		<a href="/the-front-nine/?listingKey=<?php echo $frontninedata->ListingKey ?>"><?php echo $frontninedata->ListingTitle ?></a>
				<br/>
				<?php echo $frontninedata->ListingDescription; ?><br/>
				$ <?php echo number_format((string)$frontninedata->ListPrice) ?> USD<br/>
				<h3 class="community-address"><?php echo $listing_address->FullStreetAddress ?>, <?php echo $listing_address->City ?>, <?php echo $listing_address->StateOrProvince ?>, <?php echo $listing_address->Country ?></h3>
	    	</li>
	<?php if($firstkey == ''){ $firstkey = $key; } ?>
	
	<?php } ?>
</ul>



@endsection