@layout('layouts.master')

<?php 
/*
 * Template Name: Scorecard Page Template 
 */
?>

<?php 
	wpfp_list_favorite_posts_explode();
	$type = "";
	$type = mysql_real_escape_string(trim($_GET['type']));
?>

@section('left-section')
	@if($type == 'communities' || $type == 'destinations')
		<?php $scorecard_class->display_favorite_comdest_list($type, $favorite_post_ids); ?>
	@elseif($type == 'agents')
		<?php $scorecard_class->display_favorite_agent_list($type, $favorite_agent_ids); ?>
	@elseif($type == 'properties')
		<?php $scorecard_class->display_favorite_properties_list($type, $favorite_properties); ?>
	@elseif($type == 'videos')
	@endif
@endsection



@section('main-content')
	@if($type == 'communities' || $type == 'destinations')
		@if(isset($_GET['id']))
			<?php $scorecard_class->display_favorite_comdest_main_content_with_id($type, $favorite_post_ids); ?>
		@else
			<?php $scorecard_class->display_favorite_comdest_main_content($type, $favorite_post_ids); ?>
		@endif
	@elseif($type == 'agents')
		@if(isset($_GET['id']))
			<?php $scorecard_class->display_favorite_agent_main_content($type, $favorite_agent_ids); ?>
		@else
			<?php $scorecard_class->display_favorite_agent_main_content($type, $favorite_agent_ids); ?>
		@endif
	@elseif($type == 'properties')
		<?php $scorecard_class->properties_main_content($type, $favorite_properties); ?>
	@elseif($type == 'videos')
	@endif
@endsection


@section('tab-links')
	@if($type == 'communities')
		<div id="page-links">
		    <?php get_template_part('includes/tab/tab','links' ); ?>
		</div>
	@elseif($type == 'destinations')
		<div id="page-links">
		    <?php get_template_part('includes/tab/tab','destination-links' ); ?>
		</div>
	@elseif($type == 'agents')
	@elseif($type == 'properties')
		<div id="page-links">
	        <ul class="nav nav-tabs">
	            <li class="active"><a href="#property-detail" data-toggle="tab">Property Details</a></li>
	            <li><a href="#photos" data-toggle="tab">Photos</a></li>
	            <li><a href="#location" data-toggle="tab" class="map">Location Map</a></li>
	            <li><a href="#agent" data-toggle="tab">Agent Profile</a></li>
	            <!--<li><a href="#video" data-toggle="tab">Video</a></li>-->
	        </ul>
	    </div>
	@elseif($type == 'videos')
	@endif
@endsection



@section('right-section')
	@if($type == 'communities' || $type == 'destinations')
		@if(isset($_GET['id']))
			<?php $scorecard_class->comdest_favorite_right_content_with_id($type, $favorite_post_ids) ?>
		@else
			<?php $scorecard_class->comdest_favorite_right_content($type, $favorite_post_ids); ?>
		@endif
	@elseif($type == 'agents')
		@if(isset($_GET['id']))
			<?php $scorecard_class->display_favorite_agent_right_content($type, $favorite_agent_ids); ?>
		@else
			<?php $scorecard_class->display_favorite_agent_right_content($type, $favorite_agent_ids); ?>
		@endif
	@elseif($type == 'properties')
		<?php $scorecard_class->property_right_content($type, $favorite_properties); ?>
	@elseif($type == 'videos')
	@endif
@endsection


