@layout('layouts.master')

<?php
	/*
	 * Template Name: Community Blog Template
	 */
?>

@section('main-content')

	@if(isset($_GET['id']))
		<?php $community_id = $_GET['id']; ?>

		<div id="mid-col-main">
			<?php 
                $args = array(
                    'post_type' => 'communities',
                    'post__in' => array($community_id),
                    'post_status' => 'publish'
                );
            ?>

            @wpquery($args)
            	<div class="img_wrap">
                    @if(has_post_thumbnail())
                    	{{ the_post_thumbnail('medium') }}
                    @endif
                </div>
                <h1 class="post-title">{{ the_title() }} Blog</h1>
				<?php
					// loop posts within particular category
                    $numbr = 5;

                    $paged = 1;

                    $meta_query['meta_query'] = array('relation' => 'OR');
                    $meta_query['meta_query'][0] = 
                        array(
                              'key' => 'communitycovered',
                              'value' => ':"'.$community_id.'";',
                              'compare' => 'LIKE'
                              );

                      
                    $args = array(
                        'post_type' => 'post',
                        'posts_per_page'   => 3,
                        'post_status' => 'publish',
                        'paged' => $paged,
                        'orderby'          => 'post_date',
                        'order'            => 'DESC',
                        'meta_query' => $meta_query['meta_query']
                    );

					$q2 = new WP_Query( $args );
                        if($q2->have_posts()):
                            while($q2->have_posts()): $q2->the_post();
					?>
						<div class="newscontent">
	                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
	                            <h3><?php the_title(); ?></h3>
	                        </a>
	                        <span class="sbdate"><?php echo get_the_date(); ?> | <?php echo get_the_author();?></span>
	                        <p><?php the_excerpt();?> </p>
	                    </div>
	                    <div class="clearthis"></div>
					<?php
							endwhile;
					?>
						<!-- START PAGINATION -->
						<div class="newspagination">
		                    <?php
		                        $big = 999999999; // need an unlikely integer
		                                            
		                          echo paginate_links( array(
		                            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		                            'format' => '?paged=%#%',
		                            'current' => max( 1, get_query_var('paged') ),
		                            'show_all' => 'true',
		                            'prev_text'    => __('<'),
		                            'next_text'    => __('>'),
		                            'total' => $the_query->max_num_pages
		                          ) );
		                           
		                    ?>
	                    </div>
                    	<!-- END PAGINATION -->
                    	<div class="clearthis"></div>
					<?php
						else:
                            echo "<p>Sorry, there are no posts under this community.</p>";
						endif;
						wp_reset_postdata();
		                wp_reset_query();
					?>
            @wpempty
            	 <p>Sorry, the community that you are looking for does not exists.</p>
            @wpend
		</div>


	@else
		<h3>Community id is required.</h3>
	@endif

@endsection



