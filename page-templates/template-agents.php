@layout('layouts.master')

<?php 
/*
 * Template Name: Agents Template 
 */
?>


@section('left-section')
	<div class="sr-headline row">
	@if(isset($_GET['acountry']) && isset($_GET['astate']) && isset($_GET['acity']))
		<h2>Search Results</h2>
	@else
		<h2>List of Agents</h2>
	@endif

		<?php $args = $agent_class->get_agent_args2(); ?>

		<?php
			$the_query= new WP_User_Query( $args );

            $matches=0;
            if( ! empty( $the_query->results )){
                $matches= $the_query->get_total();
            } 
		?>
		<div class="sub-head">{{ $matches }} matches</div>
	</div>
	<div id="search-results-box" class="agent-page">
		@if(isset($_GET['acountry']) && isset($_GET['astate']) && isset($_GET['acity']) && is_user_logged_in())
	    	<input type="text" class="save-search-title" style="width: 99%;margin-top: 5px;" placeholder="Name your search..." link="<?php echo $_SERVER['REQUEST_URI']; ?>" />
	    	<a href="#" class="sr-edit" data-target="#myModala" data-toggle="modal">edit search</a> <a href="#" class="sr-save">save search</a>
	    @endif
	    <?php $pstc=1; ?>

	    <?php $args = $agent_class->get_agent_args2(); ?>

		<?php $the_query= new WP_User_Query( $args ); ?>

	    <?php
	    	 if( ! empty( $the_query->results )){
	    	 	foreach ( $the_query->results as $user ) {
                    $agent_id = $user->ID;
                    $agent_email = get_the_author_meta('user_email',$agent_id); 
                    $profile_pic = get_field('profile_picture','user_'.$agent_id);
        ?>
					<div class="search-item-b">
						@if ( $profile_pic ) 
							{{ wp_get_attachment_image( $profile_pic['id'], $size = 'thumbnail') }}
						@else
							<img src="<?php bloginfo('template_directory');?>/img/default_avatar.jpg" class="s-property-thumb img-responsive" />
						@endif
						<div class="s-property-desc">
                                <p><div class="s-property-title"><?php echo $user->display_name; ?></div></p>
                                <p><?php the_field('agentcontact','user_'.$agent_id); ?></p>
                                <p><?php the_field('agentbrokerage','user_'.$agent_id);?></p>
                                <p><a href="mailto:<?php echo $agent_email; ?>">Email Agent</a></p>
                                <?php if(($country_id != "") || ($state_id != "") || ($city_id != "")) {  ?>
                                <p><a href="<?php echo site_url();?>/agents/?id=<?php echo $agent_id;?>&acountry=<?php echo $country_id;?>&astate=<?php echo $state_id; ?>&acity=<?php echo $city_id; ?>">View Details »</a></p>
                                <?php } else { ?>
                                <p><a href="<?php echo site_url();?>/agents/?id=<?php echo $agent_id;?>">View Details »</a></p>
                                <?php } ?>
                        </div>
                        <div class="clearthis"></div>
					</div>
        <?php 
        		}
        	} else {
        		echo "<p class='center'>No agents found.</p>";
        	}
        ?>
    </div>
@endsection



@section('tab-links')

@endsection


@section('main-content')
	@if( isset($_GET['id']) && is_numeric($_GET['id']) )
		<?php
			$agent_id = mysql_real_escape_string(trim($_GET['id']));
			$aux = get_userdata( $agent_id );
		?>

		@if($aux==false)
			<h3>User does not exists.</h3>
		@else
		<div id="mid-col-main">
			<div class="featured-image">
				<div class="profile-box">
					<div class="apic">
                        <?php
                            $profile_pic = get_field('profile_picture','user_'.$agent_id);
                            $size = 'full';
                        ?>
                            @if ( $profile_pic ) 
                                 <img id='c_agentpicture' src='<?php echo $profile_pic['sizes']['thumbnail']; ?>' style='width: 150px;height: 150px;' class='s-property-thumb img-responsive'/>
                            @else
                            	<img id="c_agentpicture" src="<?php bloginfo('template_directory');?>/img/default_avatar.jpg" alt="agent" title="agent" class="agentpic" />
                        	@endif
                    </div>
                    <div class="profile-description">
						<h3 id="c_agentname" class="agent-name"><?php echo get_the_author_meta('display_name',$agent_id); ?></h3>
                        <div id="c_agentposition" ><?php the_field('agentposition','user_'.$agent_id);?>, <?php the_field('agentdesignations','user_'.$agent_id);?></div>
                        <div ><?php the_field('agentcontact','user_'.$agent_id);?> | <a href="mailto:<?php echo get_the_author_meta('user_email', $agent_id); ?>">Email Me</a></div>
                        <br />
                        <div><?php the_field('agentbrokerage','user_'.$agent_id);?></div>
                        <div><?php the_field('agentbrokerageaddress','user_'.$agent_id);?></div>
                        <div id="c_agentbrokeragecity"><?php the_field('agentbrokeragecity','user_'.$agent_id);?>, <span id="c_agentbrokeragestate"><?php the_field('agentbrokeragestate','user_'.$agent_id);?></span> <?php the_field('agentbrokeragezipcode','user_'.$agent_id);?></div>
                        <br />
                        <div class="special-exp">Years Experience:   <span class="blackthis"><?php the_field('agentexperience','user_'.$agent_id);?></span></div>
                        <div class="special-exp">Community Specialist:</div>
                        <?php

                            $view_agent = get_the_author_meta('display_name',$agent_id);
                            $args = array(
                                'showposts' => -1, 
                                'post_type' => 'communities',                            
                                'orderby'          => 'post_date',
                                'order'            => 'DESC',
                                'post_status'      => 'publish'
                            );

                            $pstc=1;
                        ?>
							
                        	@wpquery( $args )
                        		<?php $agents = get_field('communityagents'); ?>
                        		@if($agents)
									<?php 
										foreach($agents as $agent) :
											$agent_name = $agent['display_name'];

											if($agent_name == $view_agent) {
									?>
											<div>- <a href="{{ the_permalink() }}">{{ the_title() }}</a></div>
									<?php
											}
										endforeach;
									?>
                        		@endif
                        	@wpempty
                        	@wpend
                    </div>
                    <div class="clearthis"></div>
                                
                    <div class="biography-box">
                        <h4 class="bio">Biography</h4>
                        <?php the_field('agentbio','user_'.$agent_id);?>
                    </div>

				</div>
			</div>
		</div>
		@endif
	@else
	<div id="mid-col-main">
		<?php $args = $agent_class->get_agent_args(1); ?>

		<?php
			$agent_query2 = new WP_User_Query( $args);

			if(! empty( $agent_query2->results )) {
				foreach($agent_query2->results as $user2) {

                    $agent_email = get_the_author_meta('user_email',$user2->ID); 
                    $agent_id = $user2->ID;
        ?>

        	<div class="featured-image">
				<div class="profile-box">
					<div class="apic">
                        <?php
                            $profile_pic = get_field('profile_picture','user_'.$agent_id);
                            $size = 'full';
                        ?>
                            @if ( $profile_pic ) 
                                 <img id='c_agentpicture' src='<?php echo $profile_pic['sizes']['thumbnail']; ?>' style='width: 150px;height: 150px;' class='s-property-thumb img-responsive'/>
                            @else
                            	<img id="c_agentpicture" src="<?php bloginfo('template_directory');?>/img/default_avatar.jpg" alt="agent" title="agent" class="agentpic" />
                        	@endif
                    </div>
                    <div class="profile-description">
						<h3 id="c_agentname" class="agent-name"><?php echo get_the_author_meta('display_name',$agent_id); ?></h3>
                        <div id="c_agentposition" ><?php the_field('agentposition','user_'.$agent_id);?>, <?php the_field('agentdesignations','user_'.$agent_id);?></div>
                        <div ><?php the_field('agentcontact','user_'.$agent_id);?> | <a href="mailto:<?php echo get_the_author_meta('user_email', $agent_id); ?>">Email Me</a></div>
                        <br />
                        <div><?php the_field('agentbrokerage','user_'.$agent_id);?></div>
                        <div><?php the_field('agentbrokerageaddress','user_'.$agent_id);?></div>
                        <div id="c_agentbrokeragecity"><?php the_field('agentbrokeragecity','user_'.$agent_id);?>, <span id="c_agentbrokeragestate"><?php the_field('agentbrokeragestate','user_'.$agent_id);?></span> <?php the_field('agentbrokeragezipcode','user_'.$agent_id);?></div>
                        <br />
                        <div class="special-exp">Years Experience:   <span class="blackthis"><?php the_field('agentexperience','user_'.$agent_id);?></span></div>
                        <div class="special-exp">Community Specialist:</div>
                        <?php

                            $view_agent = get_the_author_meta('display_name',$agent_id);
                            $args = array(
                                'showposts' => -1, 
                                'post_type' => 'communities',                            
                                'orderby'          => 'post_date',
                                'order'            => 'DESC',
                                'post_status'      => 'publish'
                            );

                            $pstc=1;
                        ?>
							
                        	@wpquery( $args )
                        		<?php $agents = get_field('communityagents'); ?>
                        		@if($agents)
									<?php 
										foreach($agents as $agent) :
											$agent_name = $agent['display_name'];

											if($agent_name == $view_agent) {
									?>
											<div>- <a href="{{ the_permalink() }}">{{ the_title() }}</a></div>
									<?php
											}
										endforeach;
									?>
                        		@endif
                        	@wpempty
                        	@wpend
                    </div>
                    <div class="clearthis"></div>
                                
                    <div class="biography-box">
                        <h4 class="bio">Biography</h4>
                        <?php the_field('agentbio','user_'.$agent_id);?>
                    </div>

				</div>
			</div>
			
		

		<?php
				}
			} else {
		?>
			<div class="col-md-12">
				<p class='center'>No agent found.</p>
			</div>
		<?php
			}
		?>
	</div>
	@endif
@endsection



@section('right-section')
	<div id="sidebar-r" class="row">
		@if( isset($_GET['id']) && is_numeric($_GET['id']) )
			<?php 
				$agent_id = trim($_GET['id']);
				$aux = get_userdata( $agent_id );
			?>
			@if($aux==false)
				<h3>User does not exists.</h3>
			@else
				<?php $agent_email = get_the_author_meta('user_email',$agent_id); ?>
				<div class="item-box">
                	<a href="#"><img src="<?php bloginfo('template_directory');?>/img/contact-icon.png" class="sidebar-r-ico" /></a>
                	<div class="sidebar-r-text">
                		<a href="mailto:{{ $agent_email }}">Contact Agent</a>
                	</div>
                	<div class="clearthis"></div>
                </div>

                <div class="item-box">
                	<a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon1.png" class="sidebar-r-ico" /></a>
                	<div class="sidebar-r-text">
                		<a href="<?php bloginfo('url'); ?>/property-search/?agentid=<?php echo $agent_id; ?>&agent-show-property=true">See All My Listings</a>
                	</div>
                	<div class="clearthis"></div>
                </div>

                <div class="item-box">
                	<a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon3.png" class="sidebar-r-ico" /></a>
                	<div class="sidebar-r-text">
                		<a href="#">Get Answers</a>
                	</div>
                	<div class="clearthis"></div>
                </div>

				@if ( is_user_logged_in() )

	            <div class="scorecard">
	                <div class="scorecard-content">
	                    <?php wpfp_agent_link(0, "", 0, array( 'agent_id' => $agent_id )) ?>
	                </div>
	            </div>
	            
	            @else
	            	<br/>
	           	@endif

	            <div class="more-links">
	                <a href="#" onclick="window.print();return false"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-print.png" alt="Print" class="ml-iconf" /></a>
	                <a href="mailto:<?php echo $agent_email; ?>"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-mail.png" alt="Mail" class="ml-icon" /></a>
	                <a href="#" data-toggle="modal" data-target="#shareModal"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-share.png" alt="Share" class="ml-icon" /></a>
	            </div>

	            <div class="e-agentbt"><a href="mailto:<?php echo $agent_email; ?>">Email Agent</a></div>


			@endif
		@else
			<?php $args = $agent_class->get_agent_args(1); ?>

			<?php 
				 $the_query= new WP_User_Query( $args );

				 if( ! empty( $the_query->results )){
                    foreach ( $the_query->results as $user ) {
                    	$agent_id = $user->ID;
                        $agent_email = get_the_author_meta('user_email',$agent_id); 
                        $profile_pic = get_field('profile_picture','user_'.$agent_id);
			?>
					<div class="item-box">
	                	<a href="#"><img src="<?php bloginfo('template_directory');?>/img/contact-icon.png" class="sidebar-r-ico" /></a>
	                	<div class="sidebar-r-text">
	                		<a href="mailto:{{ $agent_email }}">Contact Agent</a>
	                	</div>
	                	<div class="clearthis"></div>
	                </div>

	                <div class="item-box">
	                	<a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon1.png" class="sidebar-r-ico" /></a>
	                	<div class="sidebar-r-text">
	                		<a href="<?php bloginfo('url'); ?>/property-search/?agentid=<?php echo $agent_id; ?>&agent-show-property=true">See All My Listings</a>
	                	</div>
	                	<div class="clearthis"></div>
	                </div>

	                <div class="item-box">
	                	<a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon3.png" class="sidebar-r-ico" /></a>
	                	<div class="sidebar-r-text">
	                		<a href="#">Get Answers</a>
	                	</div>
	                	<div class="clearthis"></div>
	                </div>

					@if ( is_user_logged_in() )

		            <div class="scorecard">
		                <div class="scorecard-content">
		                    <?php wpfp_agent_link(0, "", 0, array( 'agent_id' => $agent_id )) ?>
		                </div>
		            </div>
		            
		            @else
		            	<br/>
		           	@endif

		            <div class="more-links">
		                <a href="#" onclick="window.print();return false"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-print.png" alt="Print" class="ml-iconf" /></a>
		                <a href="mailto:<?php echo $agent_email; ?>"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-mail.png" alt="Mail" class="ml-icon" /></a>
		                <a href="#" data-toggle="modal" data-target="#shareModal"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-share.png" alt="Share" class="ml-icon" /></a>
		            </div>

		            <div class="e-agentbt"><a href="mailto:<?php echo $agent_email; ?>">Email Agent</a></div>

			<?php
					}
				} else {
			?>
				<div class="col-md-12">
					<p class='center'>No agent found.</p>
				</div>
			<?php
				}
			?>
		@endif
	</div>
@endsection









