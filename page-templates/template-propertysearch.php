@layout('layouts.master')

<?php 
/*
 * Template Name: Property Search Template 
 */


if (isset($_REQUEST['pscountry'])) {
    $_SESSION['property-search'] = serialize($_REQUEST);
}

$order_search_result = (isset($_SESSION['order_search_result'])?$_SESSION['order_search_result']:'desc');
list($listing, $price) = property_search($order_search_result);
$mylisting = array();

if(isset($_GET['listingKey']) && isset($listing[$_GET['listingKey']])) {
    $mylisting = $listing[$_GET['listingKey']];
} else {
    $array_keys = array_keys($price);
    $mylisting = $listing[$array_keys[0]];
}
//print_r($mylisting);
$propertyimg = (string)(is_array($mylisting->Photos->Photo)?$mylisting->Photos->Photo[0]->MediaURL:$mylisting->Photos->Photo->MediaURL);

$agent = get_agent_by_listingkey(((string)$mylisting->ListingKey));


$community = get_community_by_listing_key((string)$mylisting->ListingKey);
$community_id = "#";
$community_name = "Not Set";
$community_link = '#';

if(!empty($community)) {
    $community_id = $community->ID;
    $community_name = $community->post_title;
    $community_link = get_bloginfo('url').'?p='.$community->ID;
} 

if($mylisting) {
    $listing_address = $mylisting->Address->children('commons', true);
}
?>

@section('left-section')
	<div class="sr-headline row">
    	@if(isset($_GET['comproperty-search']))
            <h2>Community List Search Results</h2>
        @elseif(isset($_GET['destproperty-search']))
            <h2>Destination List Search Results</h2>
        @elseif(isset($_GET['showupdatedproperty']))
            <h2>Updated Property Listings</h2>
        @elseif(isset($_GET['shownewproperty']))
            <h2>New Property Listings</h2>
        @elseif(isset($_GET['viewlistings']))
            <h2>Agent List Search Results</h2>
        @else
            <h2>Search Results</h2>
        @endif
    	<div class="sub-head">{{ number_format(count($price)) }} matches</div>
    </div>
	
	<div id="search-results-box">
		<form id="hayme_order_search_form">
			<div class="orderby">
				Sort by: 
				<select class="selectbox" name="orderby">
					<option value="desc">Price: Descending</option>
                    <option value="asc">Price: Ascending</option>
				</select>
            </div>
            <input type="hidden" name="action" value="hayme_order_search_result" />
            	<?php foreach($_GET as $key => $value): ?>
            		<input type="hidden" name="{{ $key }}" value="{{ $value }}" />
            	<?php endforeach; ?>
		</form>

		<br />
        @if(isset($_GET['pscountry']) && isset($_GET['psstate']) && isset($_GET['pscity']) && is_user_logged_in())
            <input type="text" class="save-search-title" style="width: 99%;margin-top: 5px;" placeholder="Name your search..." link="<?php echo $_SERVER['REQUEST_URI']; ?>" />
            <a href="#" class="sr-edit" data-target="#myModalp" data-toggle="modal" >edit search</a> <a href="#" class="sr-save">save search</a>
        @endif

        <input type="hidden" id="hayme_order_search_result" value="{{ $order_search_result }}"/>
        <div id="search-results-ordered" style="overflow:scroll">Loading result..</div>
	</div>
@endsection



@section('tab-links')
	<div id="page-links">
	    <ul class="nav nav-tabs">
	        <li class="active"><a href="#property-detail" class="map" data-toggle="tab">Property Details</a></li>
	        <li><a href="#photos" data-toggle="tab">Photos</a></li>
	        <li><a href="#location" data-toggle="tab" class="map">Location Map</a></li>
	        <li><a href="#agent" data-toggle="tab">Agent Profile</a></li>
	        <!--<li><a href="#video" data-toggle="tab">Video</a></li>-->
	    </ul>
	</div>
@endsection



@section('main-content')
	<div id="mid-col-main">
        @if($mylisting)
		<div class="featured-image">
			<div class="tab-content">
                <!-- property-detail -->
                <div class="tab-pane active" id="property-detail">
                    <div class="featured-content">
                        <div class="s-property-img-big" style="background: url(<?php echo $propertyimg ?>) no-repeat center center; background-size: cover;">
                            <!-- <img src="" title="big" class="img-responsive" /> -->
                        </div>
                        <div class="s-property-map">
                            <!--<img src="<?php bloginfo('template_directory');?>/img/sr-propertymap-big-img.jpg" title="map" class="img-responsive" />-->
                            <div class="acf-map">
                                <div class="marker img-responsive" data-lat="<?php echo (string)$mylisting->Location->Latitude; ?>" data-lng="<?php echo (string)$mylisting->Location->Longitude; ?>"></div>
                            </div>
                        </div>
                        <div class="clearthis"></div>
                    </div>
                    <div class="text-content">
                        <h2 class="community-name"><?php echo (string)$mylisting->ListingTitle ?></h2>
                        <h3 class="community-address"><?php echo $listing_address->FullStreetAddress ?>, <?php echo $listing_address->City ?>, <?php echo $listing_address->StateOrProvince ?>, <?php echo $listing_address->Country ?></h3>
                        <div class="description-tbl noborders">
                            <span class="item-price">$ <?php echo number_format((string)$mylisting->ListPrice) ?> USD</span>
                            <select class="selectbox" id="convert-item-price">
                                <option value="USD">U.S. Dollars</option>     
                                <option value="SGD">S.G. Dollars</option>
                            </select>
                            <input type="hidden" id="current-convertion" value="USD"/>
                            <input type="hidden" id="current-price" value="<?php echo (string)$mylisting->ListPrice ?>"/>
                        </div>
                        <div class="property-detail-box">
                            <div class="sp-col-1">
                                <div class="lefty">Community:</div><div class="righty"><?php echo $community_name; ?></div> <div class="clearthis"></div>
                                <div class="lefty">Bedrooms: </div>     <div class="righty"><?php echo (string)$mylisting->Bedrooms ?></div> <div class="clearthis"></div>
                                <div class="lefty">Baths: </div>        <div class="righty"><?php echo (string)$mylisting->Bathrooms ?></div> <div class="clearthis"></div>
                                <div class="lefty">Partial Baths: </div>    <div class="righty"><?php echo (string)$mylisting->PartialBathrooms ?></div> <div class="clearthis"></div>
                                <div class="lefty">Property Type: </div>   <div class="righty"><?php echo (string)$mylisting->PropertyType ?></div>  <div class="clearthis"></div>
                                <!-- <div class="lefty">Square Footage: </div>   <div class="righty">11,758</div>  <div class="clearthis"></div> -->
                                <div class="lefty">Acres: </div>        <div class="righty"><?php echo (string)$mylisting->LotSize ?></div ><div class="clearthis"></div>
                            </div>
                            <div class="sp-col-2">
                                <div class="lefty">Agent: </div>        <div class="righty"><?php echo $agent['display_name'] ? $agent['display_name'] : 'Not Set' ?></div ><div class="clearthis"></div>
                                <div class="lefty">&nbsp;</div>     <div class="righty regularfont"><?php the_field('agentposition','user_'.$agent['ID']); ?></div ><div class="clearthis"></div>
                                <div class="lefty">&nbsp;</div>     <div class="righty regularfont"><?php the_field('agentcontact','user_'.$agent['ID']); ?></div ><div class="clearthis"></div>
                            </div>
                            <div class="clearthis"></div>
                        </div>
                        <div class="propery-full-desc">
                            <h3>Property Description:</h3>
                            <p><?php echo (string)$mylisting->ListingDescription ?></p>
                        </div>                                    
                    </div>
                </div>
                <!-- end property-detail tab -->

                <div class="tab-pane" id="photos">
                    <h2 class="community-name"><?php echo (string)$mylisting->ListingTitle ?></h2>
                        <h3 class="community-address"><?php echo $listing_address->FullStreetAddress ?>, <?php echo $listing_address->City ?>, <?php echo $listing_address->StateOrProvince ?>, <?php echo $listing_address->Country ?></h3>
                    <div class="image-box">
                        <div id="gallerymainimg">
                            <div class="picnt">

                            </div>
                        </div>
                        <script>
                            jQuery(document).ready( function($){
                                
                                var imgsrc=$("#wpsimplegallery_container ul li:first-child a").attr('href');

                                console.log(imgsrc);
                                if(imgsrc == undefined) {
                                    imgsrc = stylesheet_url+'/img/img_not_available.jpg';
                                }
                                $(".picnt").html('<img src="'+imgsrc+'" />');
                                $("#wpsimplegallery_container ul li a").click( function(e){

                                    var newsrc=$(this).attr('href');

                                    console.log(newsrc+" new src");
                                    jQuery(".picnt").fadeOut('slow',function(){
                                        jQuery(".picnt img").attr('src',newsrc);
                                        jQuery(this).fadeIn('slow');
                                       
                                    });
                                    e.preventDefault();
                                });
                            });
                        </script>
                        <style>
                        #wpsimplegallery_container .clearfix {
                            zoom: 1;
                            font-size: 0;
                        }
                        #gallerymainimg img { max-height: none;}
                        #mid-col-main .indv-thumbnails img, #mid-col-main .image-box img { box-shadow: none;}
                        </style>
                        <div id="wpsimplegallery_container">
                            <ul id="wpsimplegallery" class="clearfix">
                                <?php if(is_array($mylisting->Photos->Photo)): ?>
                                <?php foreach($mylisting->Photos->Photo as $ph): ?>
                                <li><a href="<?php echo (string)$ph->MediaURL ?>"><img src="<?php echo (string)$ph->MediaURL ?>" alt="<?php echo basename((string)$ph->MediaURL) ?>" rel="wpsimplegallery_group_184"/></a></li>
                                <?php endforeach; ?>
                                <?php else: ?>
                                <?php foreach($mylisting->Photos->Photo as $photos): ?>
                                    <li><a href="<?php echo ((string)$photos->MediaURL); ?>"><img src="<?php echo ((string)$photos->MediaURL); ?>" alt="<?php echo basename((string)$photos->MediaURL); ?>" rel=""/></a></li>
                                <?php endforeach; ?>
                                
                                <?php endif; ?> 
                            </ul>
                        </div>
                        <div class="clearthis"></div>
                    </div>
                </div>
                <!-- end photos tab -->

                <div class="tab-pane" id="location">
                    <div class="text-content">
                        <h2 class="community-name"><?php echo (string)$mylisting->ListingTitle ?></h2>
                        <h3 class="community-address"><?php echo $listing_address->FullStreetAddress ?>, <?php echo $listing_address->City ?>, <?php echo $listing_address->StateOrProvince ?>, <?php echo $listing_address->Country ?></h3>
                    </div>

                    <div class="acf-map">
                        <div class="marker" data-lat="<?php echo (string)$mylisting->Location->Latitude; ?>" data-lng="<?php echo (string)$mylisting->Location->Longitude; ?>"></div>
                    </div>
                </div>
                <!-- end location tab -->

                <div class="tab-pane" id="agent">
                    <div class="text-content">
                        <h2 class="community-name"><?php echo (string)$mylisting->ListingTitle ?></h2>
                        <h3 class="community-address"><?php echo $listing_address->FullStreetAddress ?>, <?php echo $listing_address->City ?>, <?php echo $listing_address->StateOrProvince ?>, <?php echo $listing_address->Country ?></h3>
                    </div>

                    <?php //print_r($agent) ?>
                    <?php if($agent): ?>
                    <div class="profile-box">
                        <div class="apic">
                            <?php
                                $profile_pic = get_field('profile_picture','user_'.$agent['ID']);
                                $size = 'full';

                                if ( $profile_pic ) {
                                     echo "<img id='c_agentpicture' src='".$profile_pic['sizes']['thumbnail']."' style='width: 150px;height: 150px;' class='s-property-thumb img-responsive'/>";
                                } else {
                            ?>
                                <img id="c_agentpicture" src="<?php bloginfo('template_directory');?>/img/default_avatar.jpg" alt="agent" title="agent" class="agentpic" />
                            <?php
                                }
                            ?>
                        </div>

                        <!-- <img src="http://50.87.50.55/~finestg1/wp-content/themes/golf/img/karen-baldwin-profile-pic.jpg" alt="agent" title="agent" class="agentpic"> -->
                        <div class="profile-description">
                            <h3 class="agent-name"><?php echo $agent['display_name'] ?></h3>
                            <div><?php the_field('agentposition','user_'.$agent['ID']);?>, <?php the_field('agentdesignations','user_'.$agent['ID']);?></div>
                            <div><?php the_field('agentcontact','user_'.$agent['ID']);?> | <a href="mailto:<?php echo get_the_author_meta('user_email', $agent['ID']); ?>">Email Me</a></div>
                            <br>
                            <div><?php the_field('agentbrokerage','user_'.$agent['ID']);?></div>
                            <div><?php the_field('agentbrokerageaddress','user_'.$agent['ID']);?></div>
                            <div><?php the_field('agentbrokeragecity','user_'.$agent['ID']);?>, <span id="c_agentbrokeragestate"><?php the_field('agentbrokeragestate','user_'.$agent['ID']);?></span> <?php the_field('agentbrokeragezipcode','user_'.$agent['ID']);?></div>
                            <br>
                            <div class="special-exp">Years Experience:   <span class="blackthis"><?php the_field('agentexperience','user_'.$agent['ID']);?></span></div>
                            <div class="special-exp">Community Specialist:</div>
                            <?php

                                $view_agent = get_the_author_meta('display_name',$agent['ID']);
                                $args = array(
                                    'showposts' => -1, 
                                    'post_type' => 'communities',                            
                                    'orderby'          => 'post_date',
                                    'order'            => 'DESC',
                                    'post_status'      => 'publish'
                                );
                                    
                                    $the_query= new WP_query( $args);
                                    //echo "before if";
                                   //print_r($the_query);
                                    $pstc=1;
                                    //echo count($posts);
                                     if($the_query->have_posts()){
                                        while ( $the_query->have_posts() ) {
                                            $the_query->the_post();


                                            $agents = get_field('communityagents');
                                            if($agents) {
                                                foreach($agents as $agent2) {
                                                    $agent_name = $agent2['display_name'];

                                                    if($agent_name == $view_agent) {
                                        ?>
                                        <div>- <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div> 

                                        <?php
                                                    }
                                                }
                                            }                                                   
                                        }

                                        wp_reset_query();
                                        wp_reset_postdata();
                                     }


                                 
                            ?>
                        </div>

                        <div class="clearthis"></div>

                        <div class="biography-box">
                            <h4 class="bio">Biography: </h4>
                            <?php the_field('agentbio','user_'.$agent['ID']);?>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
                <!-- end agent tab -->

                <!--
                <div class="tab-pane" id="video">
                    <?php //get_template_part('includes/tab/community','video-tab-content') ?>
                </div>
                -->
                <!-- end video tab -->

            </div>
		</div>
        @else 
            <p class="center">No Properties found.</p>
        @endif
	</div>
@endsection



@section('right-section')
	<div id="sidebar-r">
        @if($mylisting)
    		@if($agent)    
            	<div class="item-box">
                	<a href="#"><img src="<?php bloginfo('template_directory');?>/img/contact-icon.png" class="sidebar-r-ico" /> </a>
    	            <div class="sidebar-r-text">
    	            	<a href="mailto:<?php echo get_the_author_meta('user_email', $agent['ID']); ?>">Contact Agent</a>
    	            </div>
                	<div class="clearthis"></div>
                </div>
            @endif

            @if(isset($_GET['comunityid']) || ($community_id != '#'))
                <div class="item-box">
                	<a href="#"><img src="<?php bloginfo('template_directory');?>/img/info-icon.png" class="sidebar-r-ico" /></a>
    	            <div class="sidebar-r-text">
    	            	<a href="<?php echo $community_link; ?>">More About Community</a>
    	            </div>
                	<div class="clearthis"></div>
                </div>
            @endif

    		<div class="item-box">
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon3.png" class="sidebar-r-ico" /></a>
                 <div class="sidebar-r-text">
           			<a href="#" class="get-answer-link" data-toggle="modal" data-target="#getAnswersCommunityModal">Get Answers</a>
            		@if($_POST['gasubmit'])
    	            <script>
    	                jQuery(document).ready(function() {
    	                    jQuery('a.get-answer-link').trigger('click');
    	                });
    	            </script>
            		@endif
           		</div>
            	<div class="clearthis"></div>
            </div>

            @if(isset($_GET['comunityid']) || ($community_id != '#'))
            	<div class="item-box">
                	<a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon4.png" class="sidebar-r-ico" /></a>
                	<div class="sidebar-r-text">
    	            	<a href="<?php bloginfo('url'); ?>/community-blog/?id=<?php echo $community_id; ?>">Community Blog</a>
    	            </div>
                	<div class="clearthis"></div>
                </div>
            @endif

            @if ( is_user_logged_in() )
                <div class="scorecard">
                    <div class="scorecard-content">
                        <?php wpfp_property_link(0, "", 0, array( 'listing_key' => ((string)$mylisting->ListingKey) )) ?>
                    </div>
                </div>
            @else
    			<br/>
            @endif

            <div class="more-links">
                <a href="#" onclick="window.print();return false" ><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-print.png" alt="Print" class="ml-iconf" /></a>
                <?php if(get_field('communityemail')): ?>
                <a href="mailto:<?php the_field('communityemail')?>"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-mail.png" alt="Mail" class="ml-icon" /></a>
                <?php endif; ?>
                <a href="#" data-toggle="modal" data-target="#shareModal"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-share.png" alt="Share" class="ml-icon" /></a>
            </div>

    		<?php  share_this_items( $_SERVER['HTTP_HOST']."/property-searchs?listingKey=".$mylisting->ListingKey, "Property" ); ?>

    		@if($agent)
            	<div class="e-agentbt"><a href="mailto:<?php echo get_the_author_meta('user_email', $agent['ID']); ?>">Email Agent</a></div>
            @endif

    		<?php get_answers_community_modal($email_array); ?>

    		<div class="clearthis"></div>
        @else
            <!-- <p class="center">No Properties found.</p> -->
        @endif
    </div>
@endsection

<script type="text/javascript">
var addthis_config = {"data_track_clickback":false,"data_track_addressbar":false,"data_track_textcopy":false,"ui_atversion":"300"};
var addthis_product = 'wpp-3.1';
</script><script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=93b61cfb127e9f8afd95a2c8fdae5442"></script>




