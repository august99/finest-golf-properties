<span style="display: none;" id="c_communitylink"><?php the_permalink(); ?></span>
<?php
    if ( has_post_thumbnail() ) {

        $thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'scorecard-com-thumb' );
?>
    <img src="<?php echo $thumb[0]; ?>" style="display: none;" id="c_communitypic" class="c_communitypic" />
    
    <div class="comdest-large-photo">
        <?php the_post_thumbnail('large'); ?>
    </div>
<?php
    }else{
?>
<div class="comdest-large-photo">
 <img id="c_communitypic" src="<?php bloginfo('template_directory'); ?>/img/img_not_available.jpg" class="img-responsive" />
</div> 
 <?php
    }
?>
<div class="text-content">
<h2 class="community-name" id="c_communitytitle"><?php the_title();?></h2>
<h3 class="community-address"><?php the_field('communitylocation');?></h3>


   
    <div class="description-tbl">
    		
            <div class="lit-info lit-mini">
                <img src="<?php bloginfo('template_directory');?>/img/opened-icon.png"/>
                 <i>Opened: </i> <b>
                    <?php 
                        $opened=get_field('communitydateopened');
                        $opened=explode("-",$opened);
                        echo $opened[0];
                    ?>
                </b>
            </div>
           <div class="lit-info"><img src="<?php bloginfo('template_directory');?>/img/designer-icon.png" class="desc-icon" /><i>Designer: </i> <b><?php the_field('coursedesigner');?></b></div>
            <div class="lit-info"><img src="<?php bloginfo('template_directory');?>/img/developer-icon.png" class="desc-icon" /><i>Developer:</i> <b><?php the_field('coursedeveloper');?></b></div>
            <div class="clearthis"></div>
    </div>

	<p><?php the_field('communitydescription');?></p>

<div class="clearthis"></div>
</div>
<div class="clearthis"></div>

