<span style="display: none;" id="c_destinationlink"><?php the_permalink(); ?></span>
<?php
    if ( has_post_thumbnail() ) {

        $thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'scorecard-com-thumb' );
?>
    <img src="<?php echo $thumb[0]; ?>" style="display: none;" id="c_destinationpic" class="c_destinationpic" />
<div class="comdest-large-photo"> 
<?php the_post_thumbnail('large');  ?>
</div>
<?php
    }else{
?>
<div class="comdest-large-photo"> 
 <img id="c_destinationpic" src="<?php bloginfo('template_directory'); ?>/img/img_not_available.jpg" class="img-responsive" />
</div>
<?php
    }
?>


<div class="text-content">
    <h2 id="c_destinationtitle" class="community-name"><?php the_title();?></h2>
    <h3 class="community-address"><?php the_field('destinationlocation');?></h3>
    <?php the_field('destinationdescription'); ?>
</div>