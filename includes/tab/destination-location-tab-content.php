<div class="text-content">
    <h2 class="community-name"><?php the_title(); ?></h2>
    <h3 class="community-address"><?php the_field('destinationlocation');?></h3>

    <div class="image-box">

        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <?php
            $map= get_field('destinationlocationmap');
            $lat=$map['lat'];
            $lon=$map['lng'];
            $location=get_field('destinationlocation');
            //print_r($map);
        ?>                         
        
        <div style="">
            <?php 

            $location = get_field('destinationlocationmap');
             
            if( !empty($location) ):
            ?>
                <div class="acf-map">
                    <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
                </div>
            <?php endif; ?>
        </div>
          
    </div>
</div>