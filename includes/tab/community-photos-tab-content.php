<div class="text-content">
    <h2 class="community-name"><?php the_title();?></h2>
    <h3 class="community-address"><?php the_field('communitylocation');?></h3>

    <div class="image-box">
        <div id="gallerymainimg">
            <div class="picnt">

            </div>
        </div>
        <script>
            jQuery(document).ready( function($){
                
                var imgsrc=$("#wpsimplegallery_container ul li:first-child a").attr('href');

                console.log(imgsrc);
                if(imgsrc == undefined) {
                    imgsrc = stylesheet_url+'/img/img_not_available.jpg';
                }
                $(".picnt").html('<img src="'+imgsrc+'" />');
                $("#wpsimplegallery_container ul li a").click( function(e){

                    var newsrc=$(this).attr('href');

                    console.log(newsrc+" new src");
                    jQuery(".picnt").fadeOut('slow',function(){
                        jQuery(".picnt img").attr('src',newsrc);
                        jQuery(this).fadeIn('slow');
                       
                    });
                    e.preventDefault();
                });
            });
        </script>
        <?php echo the_content(); ?>
        <div class="clearthis"></div>
    </div>
</div>