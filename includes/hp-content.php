        	<!-- line -->
        	<div class="contents-line">
            	<div class="contents-line-inner">
            	<div class="row">
          			<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Welcome Panel')) ?>        	
            	</div>
        	</div>
          </div>
          
          <!-- end line -->
            
         	<!-- line -->
        	<div class="contents-line">
            	<div class="contents-line-inner">
                <?php $destination_page = '11';?>
            	<div class="row">
                	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    	<h2 class="heading green"><?php echo get_the_title($destination_page)?></h2>
                        <h3 class="small"><?php echo get_post_meta($destination_page, 'sub-title', true); ?></h3>
                        
                        <?php echo $my_excerpt = get_excerpt_by_id($destination_page); //$post_id is the post id of the desired post ?>
                        
                        <br />
                          
                        <div class="btn"><a href="#" data-toggle="modal" data-target="#myModal">Learn More &rarr;</a></div>
                        
					</div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 img-thumb">
                    <br />
                    <br />
                    <?php if ( has_post_thumbnail($destination_page)) { ?>
					<?php echo get_the_post_thumbnail( $destination_page, 'page-thumb' );?>
                    <?php }
                    else{?>
                        <!-- nothing goes -->
                    <?php } ?>
                    </a>          
					</div>         	
            	</div>
        	</div>
            </div>
          
          <!-- end line -->
          
          
          
         <!-- line -->
        	<div class="contents-line">
            	<div class="contents-line-inner">
                <?php $community_page = '9';?>
            	<div class="row">
                	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 img-thumb">
                    	<br /><br />
							<?php if ( has_post_thumbnail($community_page)) { ?>
                            <?php echo get_the_post_thumbnail( $community_page, 'page-thumb' );?>
                            <?php }
                            else{?>
                                <!-- nothing goes -->
                            <?php } ?>
                        
					</div>
                    
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 alignright">
                   		<h2 class="heading green">Select Community</h2>
                        <h3 class="small"><?php echo get_post_meta($community_page, 'sub-title', true); ?></h3>
                        
                         <?php echo $my_excerpt = get_excerpt_by_id($community_page); //$post_id is the post id of the desired post ?>
                         
                        <br />
                          
                        <div class="btn"><a href="<?php echo get_permalink($community_page);?>">Learn More &rarr;</a></div>
					</div>         	
            	</div>
        	</div>
            </div>
          
          <!-- end line -->
          
          	<!-- line -->
        	<div class="contents-line">
            	<div class="contents-line-inner">
                <?php $realtor = '203'; ?>
            	<div class="row">
                	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    	<h2 class="heading green"><?php echo get_the_title($realtor)?></h2>
                        <h3 class="small"><?php echo get_post_meta($realtor, 'sub-title', true); ?></h3>
                        
                       <?php echo $my_excerpt = get_excerpt_by_id($realtor); //$post_id is the post id of the desired post ?>
                        
                        <br />
                          
                        <div class="btn"><a href="<?php echo get_permalink($realtor);?>">Learn More &rarr;</a></div>
                        
					</div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 img-thumb">
                    <br />
                    <br />
                  
                  	<?php if ( has_post_thumbnail($realtor)) { ?>
                            <?php echo get_the_post_thumbnail( $realtor, 'page-thumb' );?>
                            <?php }
                            else{?>
                                <!-- nothing goes -->
                    <?php } ?>
                            
					</div>         	
            	</div>
        	</div>
            </div>
          
          <!-- end line -->
          
            <!-- line -->
        	<div class="contents-line">
            	<div class="contents-line-inner">
                <?php $property = '205'; ?>
            	<div class="row">
                	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 img-thumb">
                    	<br /><br />
                    	<?php if ( has_post_thumbnail($property)) { ?>
                            <?php echo get_the_post_thumbnail( $property, 'page-thumb' );?>
                            <?php }
                            else{?>
                                <!-- nothing goes -->
                   		 <?php } ?>
					</div>
                    
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 alignright">
                    <br />
                   		 <h2 class="heading green"><?php echo get_the_title($property)?></h2>
                        <h3 class="small"><?php echo get_post_meta($property, 'sub-title', true); ?></h3>
                        
                       <?php echo $my_excerpt = get_excerpt_by_id($property); //$post_id is the post id of the desired post ?>
                        
                        <br />
                          
                        <div class="btn"><a href="<?php echo get_permalink($property);?>">Learn More &rarr;</a></div>
					</div>         	
            	</div>
        	</div>
            </div>
          
          <!-- end line -->
          <!-- modal -->
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content boxtype">
          <div class="modal-header noborders">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title mypopuptitle" id="myModalLabel">Choose a Golf Destination</h4>
            <h5 class="modal-sub-title">Please complete the following to start your search:</h5>
          </div>
          <div class="modal-body">
            <div id="contact-popup">
                <form>
                    <div class="cdsel" id="cdcountry">
                        <div class="left-col">
                            <p><label>Country</label></p>
                        </div>
                        <div class="right-col">
                            <select id="pscountry" name="pscountry" class="psst">
                                <option value="">Select Country</option>
                                <option value="us">United States</option>
                                <option value="ca">Canada</option>
                            </select>
                        </div>
                        <div class="clearthis"></div>
                    </div>
                    <div class="clearthis"></div>

                    <div class="cdsel" id="cdstate">
                        <div class="left-col">
                            <p><label>State</label></p>
                        </div>
                        <div class="right-col">
                            <select id="psstate" name="psstate" class="psst">
                                <option value="">Select State</option>
                                <option value="ca">California</option>
                                <option value="az">Arizona</option>
                            </select>                            
                        </div>
                        <div class="clearthis"></div>
                    </div>
                    <div class="clearthis"></div>

                    <div class="cdsel" id="cdcity">
                        <div class="left-col">
                            <p><label>City</label></p>
                        </div>
                        <div class="right-col">
                            <select id="pscity" name="pscity" class="psst">
                                <option value="">Select City</option>
                                <option value="la">L.A.</option>
                                <option value="sf">San Francisco</option>
                            </select>                           
                        </div>
                        <div class="clearthis"></div>
                    </div>
                    <div class="clearthis"></div>

                    <div class="cdsel" id="cdptype">
                        <div class="left-col">
                            <p><label>Property Type</label></p>
                        </div>
                        <div class="right-col">
                            <select id="pstype" name="pstype" class="psst">
                                <option value="">Select Property Type</option>
                                <option value="co">Condominium</option>
                                <option value="ho">House</option>
                            </select>                           
                        </div>
                        <div class="clearthis"></div>
                    </div>
                    <div class="clearthis"></div>

                    <div class="" id="cdboxes">
                        <div class="left-col">
                            <p><label> </label></p>
                        </div>
                        <div class="gdtb right-col">
                            <div class="left-col">
                                <input type="text" placeholder="Minimum Price" value="">
                            </div>  
                            <div class="right-col">                             
                                <input type="text" placeholder="Maximum Price" value="">
                            </div>
                            <div class="clearthis"></div> 

                            <div class="left-col"> 
                                <input type="text" placeholder="Bedrooms" value="">
                            </div>
                            <div class="right-col">                         
                                <input type="text" placeholder="Bathrooms" value="">
                            </div>                         
                            <div class="clearthis"></div> 
                            
                            <div class="left-col">
                                <input type="text" placeholder="Minimum Sq. Ft." value="">
                            </div>
                            <div class="right-col">
                                <input type="submit" value="Submit →" class="wpcf7-form-control wpcf7-submit s-des btn">
                            </div>
                            <div class="clearthis"></div> 
                        </div>
                        <div class="clearthis"></div>
                    </div>
                    <div class="clearthis"></div>
                </form>
                <div class="clearthis"></div>
            </div>
          </div>
          <!-- <div class="modal-footer noborders">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
          -->
        </div>
    </div>  
</div>
<!-- end modal -->