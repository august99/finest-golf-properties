<!-- modal -->
<div id="myModal2" class="modal fade">

<div class="modal-dialog" id="scorecardbox">
    <div class="modal-content myscorecard">
      <div class="modal-header noborders">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      
            <div class="scoreboard-heading">
                <div class="l-col"><img src="<?php bloginfo('template_directory');?>/img/pen-icon-scorecard-heading.png" alt="my scorecard" title="my scorecard" class="myscrollcard-img" /> My scorecard</div>
                <div class="r-col">
                        <div class="r-col1"><a href="#"><img src="<?php bloginfo('template_directory');?>/img/pref-icon.png" alt="preferences" title="preferences" class="rcols-img" /> Preferences</a></div>
                        <div class="r-col2"><a href="#"><img src="<?php bloginfo('template_directory');?>/img/logout-icon.png" alt="preferences" title="preferences" class="rcols-img" />Log Off My Scorecard</a></div>
                    
                    <div class="clearthis"></div>
                </div>
                    <div class="clearthis"></div>
      </div>
      <div class="modal-body">
        <div class="pop-up-l">
            <?php $current_user = wp_get_current_user(); ?>
            <h2 class="user-greetings">Hello, <?php the_author_meta('display_name', $current_user->ID); ?></h2>
            <h3 class="subline">You have the following Recent Updates:</h3>
            <div class="recent-updates">
                <?php
                $listhub_updated_property = 0;
                if ($favorite_properties) {
                    foreach ($favorite_properties as $key) {
                        if (in_array($key, get_listhub_updated_property())) {
                            $listhub_updated_property++;
                        }
                    }
                }
                $community_class = new Communities;
                $destination_class = new Destinations;

                $new_community_arg = $community_class->get_community_shownew_args(-1, false);
                $new_community = new WP_Query($new_community_arg);
                $total_new_community = $new_community->post_count;
                wp_reset_query();

                $new_destination_arg = $destination_class->get_destination_shownew_args(-1, false);
                $new_destination = new WP_Query($new_destination_arg);
                $total_new_destination = $new_destination->post_count;
                wp_reset_query();
                ?>
                <ul>
                    <li><a href="<?php bloginfo('url'); ?>/property-search/?showupdatedproperty=true">Updated Property Listings (<?php echo $listhub_updated_property ?>)</a></li>
                    <li><a href="<?php bloginfo('url'); ?>/property-search/?shownewproperty=true">New Property Listings (<?php echo count(get_listhub_new_property()) ?>)</a></li>
                    <li><a href="<?php bloginfo('url'); ?>/golf-communities/?shownewcommunity=true">New Golf Communities (<?php echo $total_new_community ?>)</a></li>
                    <li><a href="<?php bloginfo('url'); ?>/golf-destinations/?shownewdestination=true">New Golf Destinations (<?php echo $total_new_destination ?>)</a></li>
                    <!-- <li>New Videos (3)</li>
                    <li>New Messages (5)</li> -->
                </ul>   
            </div>
            
            <a href="#"><div class="msgbtn"><span><img src="<?php bloginfo('template_directory');?>/img/mail-icon-msg-bt.png" class="msgicon" /></span>Go To My Messages</div></a>
            <h3 class="subline">You have the following Saved Searches:</h3>
            
            <div class="saved-searches">
                <ul>
                <?php if($saved_searches): ?>
                    
                        <?php 
                            $saved_searches = array_reverse($saved_searches); 

                                foreach($saved_searches as $s => $key):

                        ?>
                                <li>
                                    <a href="<?php echo get_bloginfo('url').$key['search_link']; ?>"><?php echo $key['search_name']; ?></a> 
                                    <a href="#" class="sr-remove-search" style="float: right;" link="<?php echo $key['search_link']; ?>" search-id="<?php echo $key['search_id']; ?>">x</a>
                                </li>
                                
                        <?php 
                                endforeach;
                        ?>
                <?php endif; ?>

                    <!-- <li class="newsearch-l"><a href="#">New Search</a></li> -->
                </ul>
            </div>
                        
        </div>
        <div class="pop-up-r">
        
       <!--- per section -->

            <div class="per-section">
            
                <div class="short-detail">View All <a id="desti_count_displayed" href="<?php bloginfo('url'); ?>/my-scorecard/?type=destinations"></a> | <a href="#" class="lightgrey">Edit</a></div>
                
                <div class="section-container">
                    <div class="big-logo"><img src="<?php bloginfo('template_directory');?>/img/my-destination-icon.png" /></div>

                        <div class="scrollable-content">
                        <div id="destination-content" class="scrollables">
                            <?php 
                                $ctr = 0;
                                if ($favorite_post_ids) { 
                                    //var_dump($favorite_post_ids);
                                    $favorite_post_ids = array_reverse($favorite_post_ids);

                                    $qry = array('post__in' => $favorite_post_ids, 'orderby' => 'post__in');
                                    // custom post type support can easily be added with a line of code like below.
                                    $qry['post_type'] = array('destinations');
                                    $post_in = count($qry['post__in']);
                                    query_posts($qry);

                                if($post_in != 0) {
                                    while ( have_posts() ) : $ctr++; the_post(); $picture = wp_get_attachment_image_src( get_post_thumbnail_id(), 'scorecard-com-thumb' );
                            ?>

                                        <div id="dest_ids_<?php the_ID(); ?>" class="dest-item-box">
                                            <div class="dest-img">
                                                <a href="<?php the_permalink(); ?>">
                                                    <?php 
                                                        if($picture)
                                                            echo "<img src='".$picture[0]."' style='width: 160px;height: 95px;' class='img-responsive'/>";
                                                        else
                                                            echo "<img src='".get_bloginfo('template_directory')."/img/default_avatar.jpg' style='width: 160px;height: 95px;' class='img-responsive'/>";
                                                    ?>
                                                </a>
                                             </div>
                                            <div class="dest-desc"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
                                        </div>
                            <?php   endwhile; 
                                    
                                } 
                                

                                if($ctr == 0) {
                                    echo "<span class='no_item_message'>No destinations found.</span>";
                                }

                                  wp_reset_postdata();
                                  wp_reset_query();
                                } else {
                                    echo "<span class='no_item_message'>No destinations found.</span>";
                                }
                                echo "<span id='desti_count_hidden' style='display: none;'>".$ctr."</span>";
                            ?>
                        <!-- <div class="clearthis"></div> -->
                        </div>
                        </div>
                            
                    
                    <div class="clearthis"></div>
                </div>
            
            </div>
            
       <!-- end per section -->
        
        
               <!--- per section -->
         
            <div class="per-section">
            
                <div class="short-detail">View All <a href="<?php bloginfo('url'); ?>/my-scorecard/?type=communities" id="com_count_displayed"></a> | <a href="#" class="lightgrey">Edit</a></div>
                
                <div class="section-container">
                    <div class="big-logo"><img src="<?php bloginfo('template_directory');?>/img/my-communities-icon.png" /></div>

                        <div class="scrollable-content">
                        <div id="community-content" class="scrollables">

                            <?php 
                                $ctr = 0;
                                if ($favorite_post_ids) {   
                                    //var_dump($favorite_post_ids);
                                    $favorite_post_ids = array_reverse($favorite_post_ids);

                                    $qry = array('post__in' => $favorite_post_ids, 'orderby' => 'post__in');
                                    // custom post type support can easily be added with a line of code like below.
                                    $qry['post_type'] = array('communities');
                                    $post_in = count($qry['post__in']);
                                    query_posts($qry);

                                if($post_in != 0) {
                                    
                                    while ( have_posts() ) { the_post(); $ctr++; $picture = wp_get_attachment_image_src( get_post_thumbnail_id(), 'scorecard-com-thumb' ); ?>
                                    <!-- <img style="width: 160px;"src="<?php bloginfo('template_directory');?>/img/scorecard-dest-thumb1.jpg" /> -->
                                        <div id="com_ids_<?php the_ID(); ?>" class="comm-item-box">
                                            <div class="comm-img">
                                                <a href="<?php the_permalink(); ?>">
                                                    <?php 
                                                        if($picture)
                                                            echo "<img src='".$picture[0]."' style='width: 160px;height: 95px;' class='img-responsive'/>";
                                                        else
                                                            echo "<img src='".get_bloginfo('template_directory')."/img/img_not_available.jpg' style='width: 160px;height: 95px;' class='img-responsive'/>";
                                                    ?>
                                                </a>
                                            </div>
                                            <div class="comm-desc"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
                                        </div>
                            <?php   } 
                                    
                                  } 
                                 
                                  if($ctr == 0) {
                                     echo "<span class='no_item_message'>No communities found.</span>";
                                  }
                                  
                                  
                                    } else {
                                    echo "<span class='no_item_message'>No communities found.</span>";
                                  }
                                  echo "<span id='com_count_hidden' style='display: none;'>".$ctr."</span>";
                                  wp_reset_postdata();
                                  wp_reset_query();
                            ?>

                        
                        
                              
                        <!-- <div class="clearthis"></div> -->
                        </div>
                        </div>
                            
                    
                    <div class="clearthis"></div>
                </div>
            
            </div>
            
       <!-- end per section -->
       
      <!--- per section -->
         
            <div class="per-section">
            
                <div class="short-detail">View All <a href="<?php bloginfo('url'); ?>/my-scorecard/?type=properties"><?php echo $favorite_properties?count($favorite_properties):0 ?></a> | <a href="#" class="lightgrey">Edit</a></div>
                
                <div class="section-container">
                    <div class="big-logo"><img src="<?php bloginfo('template_directory');?>/img/my-properties-icon.png" /></div>
                    <div class="scrollable-content">
                        <div class="scrollables">
                            <?php if($favorite_properties):
                                list($property, $price) = property_search('admin');
                                foreach($favorite_properties as $key):
                                    $pImg = (string)(is_array($property[$key]->Photos->Photo)?$property[$key]->Photos->Photo[0]->MediaURL:$property[$key]->Photos->Photo->MediaURL);
                                    $agent = get_agent_by_listingkey(((string)$property[$key]->ListingKey))
                                ?>
                                <div class="prop-item-box">
                                    <div class="prop-img"><a href="<?php bloginfo('url'); ?>/property-search/?listingKey=<?php echo (string)$property[$key]->ListingKey; ?>" ><img src="<?php echo $pImg ?>" width="100" /></a></div>
                                    <div class="prop-desc">
                                        <div class="prop-line1"><?php echo (string)$property[$key]->ListingTitle ?></div>
                                        <div class="prop-line2"><?php echo $property[$key]->Address->children('commons', true)->StateOrProvince ?>, <?php echo $property[$key]->Address->children('commons', true)->Country ?></div>
                                        <div class="prop-line3">$<?php echo (string)$property[$key]->ListPrice ?></div>
                                        <div class="prop-line4"><?php echo (string)$property[$key]->Bedrooms ?> Beds | <?php echo (string)$property[$key]->Bathrooms ?> Baths</div>
                                        <!-- <div class="prop-line5">8,147 SqFt</div> -->
                                        <div class="agent-l"><?php echo $agent['display_name']?'<a href="'.get_bloginfo('url').'/agents/?id='.$agent['ID'].'">'.$agent['display_name'].'</a>':'<a href="#">Not Set</a>' ?></div>
                                    </div>
                                    <div class="clearthis"></div>
                                </div>
                                <?php endforeach; else: echo "<p class='no_item_message'>No properties found.</p>"; ?>
                            <?php endif; ?>
                            <div class="clearthis"></div>
                        </div>
                    </div>
                    <div class="clearthis"></div>
                </div>
            
            </div>
            
       <!-- end per section -->
       
       
       <!--- per section -->
            <?php 
                $agent_count = 0;
                if($favorite_agent_ids) {  $favorite_agent_ids = array_reverse($favorite_agent_ids);  $agent_count = count($favorite_agent_ids); } 
            ?>
            <div class="per-section">

                <div class="short-detail">View All <a href="<?php bloginfo('url'); ?>/my-scorecard/?type=agents"><?php echo $agent_count;?></a> | <a href="#" class="lightgrey">Edit</a></div>

                <div class="section-container">
                    <div class="big-logo"><img src="<?php bloginfo('template_directory');?>/img/pref-golf-re-icon.png" /></div>

                        <div class="scrollable-content">
                        <div id="agent-container" class="scrollables">

                            <?php 

                                if($favorite_agent_ids) {
                                    $favorite_agent_ids = array_reverse($favorite_agent_ids);

                                    $args = array(
                                        'role' => 'aamrole_53ec21a063e46',
                                        'include' => $favorite_agent_ids
                                    );

                                    $the_query= new WP_User_Query( $args);
                                    //var_dump($the_query);

                                    if( ! empty( $the_query->results )){
                                        foreach ( $the_query->results as $agent ) {
                                            $agent_id = $agent->ID;
                                            $profile_pic = get_field('profile_picture','user_'.$agent_id);
                                            $agent_email = get_the_author_meta('user_email',$agent_id); 
                                            $agent_name = get_the_author_meta('display_name',$agent_id); 
                                            $agent_url = get_bloginfo('url').'/agents/?id='.$agent_id;
                                            $agent_brokerage = get_field('agentbrokerage','user_'.$agent_id);
                                            $agent_brokerage_city = get_field('agentbrokeragecity','user_'.$agent_id).", ".get_field('agentbrokeragestate','user_'.$agent_id);
                            ?>
                            
                                    <div id="agent-<?php echo $agent_id; ?>" class="agt-item-box">
                                            <div class="agt-img">
                                                <a href="<?php echo $agent_url; ?>">
                                                    <?php 
                                                        if($profile_pic)
                                                            echo "<img src='".$profile_pic['sizes']['scorecard-agent-thumb']."' style='width: 90px;height: 104px;' class='s-property-thumb img-responsive'/>";
                                                        else
                                                            echo "<img src='".get_bloginfo('template_directory')."/img/default_avatar.jpg' style='width: 90px;height: 104px;' class='s-property-thumb img-responsive'/>";

                                                    ?>
                                                </a>
                                            </div>
                                            <div class="agt-desc">
                                                <div class="agt-line1"><a href="<?php echo $agent_url; ?>"><?php echo $agent_name; ?></a></div>
                                                <div class="agt-line2"><?php echo $agent_brokerage; ?></div>
                                                <div class="agt-line3"><?php echo $agent_brokerage_city; ?></div>
                                            </div>
                                        <div class="clearthis"></div>
                                     </div>

                             <?php 
                                        } //foreachh
                                    } else {
                                        echo "<span class='no_item_message'>No agents found.</span>";
                                    }
                                } else {
                                    echo "<span class='no_item_message'>No agents found.</span>";
                                }

                                wp_reset_postdata();
                                  wp_reset_query();

                             ?>

                        <!-- <div class="clearthis"></div> -->
                        </div>
                        </div>
                            
                    
                    <div class="clearthis"></div>
                </div>
            
            </div>
            
       <!-- end per section -->
        
        
               <!--- per section -->
         
            <div class="per-section">
            
                <div class="short-detail">View All <a href="<?php bloginfo('url'); ?>/my-scorecard/?type=videos"></a> | <a href="#" class="lightgrey">Edit</a></div>
                
                <div class="section-container">
                    <div class="big-logo"><img src="<?php bloginfo('template_directory');?>/img/my-videos-icon.png" /></div>

                        <div class="scrollable-content">
                        <div id="video-content" class="scrollables">

                            <?php 

                                if($favorite_video_urls):
                                    foreach($favorite_video_urls as $urls) :
                                        $temp = explode("|",$urls);
                                        $title = $temp[0];
                                        $vid_id = $temp[1];
                            ?>
                            
                                    <div id="vid_id_<?php echo $vid_id; ?>" class="vid-item-box">
                                        <div class="vid-content"><a href="http://www.youtube.com/watch?v=<?php echo $vid_id; ?>"><img src="http://img.youtube.com/vi/<?php echo $vid_id; ?>/1.jpg" /></a></div>
                                        <div class="vid-desc"><a href="http://www.youtube.com/watch?v=<?php echo $vid_id; ?>"><?php echo $title; ?></a></div>
                                    </div>

                            <?php
                                    endforeach;
                                else:
                                    echo "<p class='no_item_message'>No videos found.</p>";
                                endif;
                            ?>
                   
                
                        </div>
                        </div>
                            
                    
                    <div class="clearthis"></div>
                </div>
            
            </div>
            


       <!-- end per section -->
        
        
        
        
        </div>
            <div class="clearthis"></div>
            
            
           <div class="scorecard-footer">
           
                <div class="left-f">
                    Copyright © 2014 Finest Media Group LLC
                </div>
                
                <div class="right-f">
                
                    <a href="#">Privacy Policy</a>  |  <a href="#">Terms  of Use</a>
                
                </div>
                
                
                <div class="clearthis"></div>
           
           </div>
            
      </div>
      <!-- <div class="modal-footer noborders">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      -->
    </div>
  </div>
  
</div>
</div>

