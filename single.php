@layout('layouts.master')


@section('main-content')


<div id="mid-col-main" class="blogsingle">
    @wpposts
        <div class="img_wrap">
            <?php the_post_thumbnail('large');  ?>
        </div>
        <h1 class="post-title"><?php the_title(); ?></h1>
        <span class="sbdate"><?php echo get_the_date(); ?> | <?php echo get_the_author();?></span>
            <?php the_content(); ?>                   
        <div class="clearthis"></div>
    @wpempty
        <p class="center">No contents found.</p>
    @wpend
</div>


@endsection