<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head();?>
    <!-- Bootstrap -->
    <link href="<?php bloginfo('template_directory');?>/css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php bloginfo('template_directory');?>/css/responsive.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory');?>/css/dropdown-menu.css">
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,100italic,300italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href="<?php bloginfo('template_directory');?>/js/custom-scrollbar/jquery.mCustomScrollbar.css" rel="stylesheet" />

   
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
    <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

    
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- scripts -->
    <!-- <script src="http://code.jquery.com/jquery-1.11.0.js"></script> -->
    <script src="<?php bloginfo('template_directory');?>/js/cufon-yui.js"></script>
    <script src="<?php bloginfo('template_directory');?>/js/Century_300.font.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="<?php bloginfo('template_directory');?>/js/bootstrap.js"></script>
    <script src="<?php bloginfo('template_directory');?>/js/tinynav.js"></script> 
    <script src="<?php bloginfo('template_directory');?>/js/custom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src='<?php bloginfo('template_directory');?>/js/jquery.customSelect.js'></script>
    <script>
      var stylesheet_url = "<?php echo get_stylesheet_directory_uri(); ?>";
      var blog_url = "<?php echo get_bloginfo('url'); ?>";
    </script>
    <?php
      if(is_home()){
    ?>
      <script src='<?php bloginfo('template_directory');?>/js/custom.js'></script>
    <?php
      }
    ?>
    
    <title><?php bloginfo('name'); ?></title>
    </head>

<body>

<div id="top-section">
	<div class="container">
    	<div class="row">
            <div class="col-xs-8 col-sm-6 col-md-5 col-lg-4 logo-col">
                <h1><a href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></a></h1>
            </div>
            
            <div class="hidden-xs hidden-sm col-md-2 col-lg-4 spacer-col">
           		&nbsp;
            </div>
            
             <div class="col-xs-12 col-sm-6 col-md-5 col-lg-4 log-col">
              <?php if(is_user_logged_in()): ?>
                <div class="scorecardbt"><a href="#" data-toggle="modal" data-target="#myModal2">my scorecard &raquo;</a></div>
              <?php endif; ?>
            	<div class="siginbt"><a href="#" data-toggle="modal" data-target="#myModalSignup">sign up </a>  /  
                
                <a href="#" data-toggle="modal" data-target="#myModalagent">Apply as an Agent</a> / 
                <!--a href="<?php bloginfo('wpurl'); ?>/wp-login.php">login</a></div-->
                <a href="#" data-toggle="modal" data-target="#myModalLogin">login</a></div>
                	<div class="clearthis"></div>
            </div>
            
         </div>  
    </div>
       <!-- navigation -->
         <div class="site-menu">
         	<div class="container">
            	<div class="row">
                
                   <?php wp_nav_menu( array( 'theme_location' => 'main-menu')); ?>
                   
            	</div>
            </div>
         </div>
       <!-- end navigation -->
       
       
       <?php if(is_front_page() ) { ?>
			 <style>.inner-content{padding:50px 20px 0 20px;}</style>
			  <!-- slideshow -->
               <div class="banner">
               <!--img src="<?php bloginfo('template_directory');?>/img/temp-slideshow.jpg" class="img-responsive" /-->
               <?php //if( function_exists('cyclone_slider') ) cyclone_slider('homepageslider'); ?>

              <video>
                <source src="<?php echo get_stylesheet_directory_uri(); ?>/includes/DesertHighlandsVideo.mp4" type="video/mp4"></source>
                asdasdsa
              </video>
               </div>
              <!-- end slideshow -->
              
              <!-- tagline -->
              <div class="tagline">
                <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('HP Tagline')) ?>   
                    <a href="#main-contents"><img src="<?php bloginfo('template_directory');?>/img/tagline-arrow.png" alt="arrow" title="arrow" /></a>
              </div>
              <!-- end tagline --> 

		<?php } else{ ?>
        <style></style>
        <!-- if no slider -->
        <?php } ?>
		
</div>


<!-- Main Contents -->
<div id="main-contents">
	<div class="inner-content container">
        		<div class="row">
                
                	<?php if(is_front_page() ) { ?>

                    	<?php include (TEMPLATEPATH . '/includes/hp-content.php'); ?>
                    
                    <?php } else{ ?>
                    <!-- nothing here -->
                    <?php } ?>