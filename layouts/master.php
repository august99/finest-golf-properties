@include('partials.header')

<div id="page-content" class="destination-page">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 left-col">
			<div class="search-results">
				@yield('left-section')
			</div>
		</div>
	    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
	    	<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 mid-col">
					@yield('tab-links')
					@yield('main-content')
				</div>

				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 right-col">
					@yield('right-section')
				</div>
			</div>
	    </div>


    <?php endwhile; else: ?>
    <?php endif; ?>
	<div class="clearfix"></div>
</div>


@include('partials.footer')