@include('partials.header')

<div id="page-content" class="searchtemplate">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mid-col">
    	<div id="mid-col-main">
			@yield('main-content')
			<div class="clearthis"></div>
		</div>
   	</div>

    <div class="clearthis"></div>
</div>

@yield('hidden-items')


@include('partials.footer')